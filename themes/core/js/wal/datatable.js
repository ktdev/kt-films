$(document).ready(function () {

    // On page load: datatable
    var table_prisoners = $('#table_films').dataTable({
        "ajax": "../../../app/mods/film/list?job=get_listfilms",
        "language": {
            "decimal": ",",
            "thousands": "."},
        "columns": [
            {"data": "titre"},
            {"data": "categorie"},
            {"data": "support"},
            {"data": "reference"},
            {"data": "functions", "sClass": "functions"}
        ],        
        "aoColumnDefs": [
            {"bSortable": false, "aTargets": [-1]}
        ],
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "oLanguage": {
            "oPaginate": {
                "sFirst": "First",
                "sPrevious": "Previous ",
                "sNext": "Next",
                "sLast": "Last",
            },
            "sLengthMenu": "Records per page: _MENU_",
            "sInfo": "Total of _TOTAL_ records (showing _START_ to _END_)",
            "sInfoFiltered": "(filtered from _MAX_ total records)"
        }
    });

});