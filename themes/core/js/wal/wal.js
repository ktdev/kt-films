$(document).ready(function() {
    
        var poster_lien = $('#poster_lien').val();
        var poster_name = $('#poster_name').val();
        var activer_suppression_affiche = false;
        
        $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover',
        html : true,
        content : '<div class="media"><a href="#" class="pull-left"><img src="'+poster_lien+'" class="media-object" alt="Sample Image"></a><div class="media-body">'+poster_name+'</div>'
        });

        
        /*.................................................................*/
        /*   Implémentation de la libraire Swipebox (affichage des photos) */
        /*.................................................................*/
        ;( function( $ ) {

            $( '.swipebox' ).swipebox();

        } )( jQuery );
        
});

/* --- MODAL DELETE -- */
$('#modalFilmDeleteConfirm').on('shown.bs.modal', function () {
     
})

$('#supprimer').click(function(e) {
    $('#ficheFilm').submit();    
});


/*---------------------*/

$('#edit-profile').submit(function(e) {
    if ($('#inputPassword').val() !== $('#inputPassword2').val()) {
        alert(T_('Attention, le mot de passe de confirmation est différent du mot de passe !'));
        e.preventDefault();
        return false;
    }
});

$('#active_del_poster').click(function(){
    
    if($('#lbl_message_del_picture').html() == '') {
       $('#lbl_message_del_picture').append('&nbsp;'+L_AFFICHE_A_SUPPRIMER); 
       $('#active_del_poster').empty()
       $('#active_del_poster').append('Désactiver la suppression')
      
       $('#del_picture').prop('checked', true);   
       //console.log('Pour suppression');
    }else{
       $('#lbl_message_del_picture').empty();
       $('#del_picture').prop('checked', false); 
       $('#active_del_poster').empty()
       $('#active_del_poster').append('Activer la suppression')
      //console.log('pas supprimer');
    }
});
    
/*.................................................................*/
/*     Customisation des Boutons "parcourir" des formulaires       */
/*.................................................................*/
$('input.custom-input-file').change(function(){
    $('.name-file').empty();
    var filename = $(this).val();
    $('.name-file').append(filename);
});

$('input.custom-input-photo').change(function(){
    $('.name-file').empty();
    var filename = $(this).val();
    $('.name-photo').append(filename);
});



