<?php

//-----------------------------------------------------------------------------
// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++    

// Si on reçoit des données du formulaire d'identification
if(isset($_POST['action']))
{
    $action = $_POST['action'];
    $user = $_POST['inputUser'];
    $passwd = $_POST['inputPassword'];

    // Tentative d'identification
    $st = KTIdentUser($user, $passwd);

    // Si identification ok
    if($st['stat']) 
    {
        // Récupération des datas du user
        $_SESSION['pseudo'] =  $st['pseudo'];
        $_SESSION['email'] = $st['email'];
        $_SESSION['actif'] = $st['actif'];
        $_SESSION['id_user'] = $st['id_user'];
        $_SESSION['su'] = $st['su'];

        // Initialiser Identify sur (1) et indiquer ainsi que l'utilisateur est identifié
        $_SESSION['IDENTIFY'] = 1;

        // Router l'utilisateur identifié vers la page index du module primaire
        $route = new Router( D_APP.DS.D_MODS.DS.D_PRIMARY.DS.'index', M_REDIR);

    }else{
        // Afficher une div d'alerte avec le message correspondant
        $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40', $st['msg'], 'alert' );

        // Initialiser Identify sur (0)  et ainsi refuser l'accès aux autres modules a tous les utilisateurs non identifiés.
        $_SESSION['IDENTIFY'] = 0;
        $_SESSION['pseudo'] = NULL;
    } 
}else{
    $action = NULL;
    $user = NULL;
    $passwd = NULL;
    $msg = NULL;
    $st = NULL;

}

// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_HOME . DS );

// Assignation du template
if($_SESSION['IDENTIFY'] == 0)
    $engine->set_file( D_HOME, 'tpl_login.htm' );
else{
    $engine->set_file( D_HOME, 'tpl_identified.htm' );
    $engine->set_var('pseudo', $_SESSION['pseudo']);

}
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Afficher le texte d'introduction
$engine->set_var('txt_welcom', $GLOBALS['G_TXT_WELCOM']); 

// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Elements a afficher et à traduire
$engine->set_var('trm-identification', T_('Identification'));
$engine->set_var('trm-user', T_('Identifiant'));
$engine->set_var('trm-souvenir', T_('Se souvenir'));
$engine->set_var('trm-identifiant', T_('Votre indentifiant'));
$engine->set_var('trm-motdepasse', T_('Votre mot de passe'));
$engine->set_var('trm-connecter', T_('Se connecter'));

$engine->set_var('trm-identification', T_('Identification'));
$engine->set_var('trm-bonjour', T_('Bonjour'));
$engine->set_var('trm-identifie-sur', T_('Vous êtes actuellement connecté sur'));
$engine->set_var('trm-action-a-realiser', T_('Quelle action souhaitez-vous réaliser ?'));
$engine->set_var('trm-vous-deconnecter', T_('Vous déconnecter'));
$engine->set_var('trm-acces-module-defaut', T_('Accéder au module par défaut'));

// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page

if(K_DEBUG)
{
    // DEBUG MODE ON FIREPHP
    $firephp = FirePHP::getInstance(K_DEBUG);  
    if(isset($firephp)) $firephp->setEnabled(K_DEBUG);
    $firephp->dump('SESSION', $_SESSION );   
} 
 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_HOME );

// Rendu du template
$engine->p( 'display' );
