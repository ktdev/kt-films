<?php

//-----------------------------------------------------------------------------
// ++++++++++++++++++++++++++++++++++++++ CONTROLER +++++++++++++++++++++++++++
// LOGOFF
// Sauvegarder la locale
$saveLocale = $_SESSION['locale'] ;
setcookie(session_name(), '', 100);
session_unset();
session_destroy();
$_SESSION = array(); 

// Nouvelle Session  
new Session();
$_SESSION['startTime'] = microtime( true );
$_SESSION['IDENTIFY'] = 0;
$_SESSION['locale'] = $saveLocale;


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_HOME . DS );

// Assignation du template
$engine->set_file( D_HOME, 'tpl_logout.htm' );
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Afficher le texte d'introduction
$engine->set_var('txt_welcom', $GLOBALS['G_TXT_WELCOM']); 

// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Elements a afficher et à traduire
$engine->set_var('trm-deconnexion', T_('Déconnexion'));
$engine->set_var('trm-a-present-deconnecte', T_('Vous êtes à présent déconnecté'));
$engine->set_var('trm-retour-homepage', T_('Retour sur la page d\'accueil'));

// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Section de d?bugage de la page
if(K_DEBUG)
{
    // DEBUG MODE ON FIREPHP
    $firephp = FirePHP::getInstance(K_DEBUG);  
    if(isset($firephp)) $firephp->setEnabled(K_DEBUG);
    $firephp->dump('SESSION', $_SESSION);
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associ?es
$engine->parse( 'display', D_HOME );

// Rendu du template
$engine->p( 'display' );


