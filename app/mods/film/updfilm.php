<?php


//-----------------------------------------------------------------------------
// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++  

//print_r($_POST);

// Si $_POST vide alors chargement des données du film pour initialiser le formulaire
if(empty($_POST)) {
    $idfilm = $_GET["id"];
    $film = getFicheFilm($idfilm);
    extract($film);
}
    

// Si $_POST modification
if(!empty($_POST['modifier'])){
    
    // Charger les données du film
    $idfilm = $_GET["id"];
    $film = getFicheFilm($idfilm);
    extract($film);
    
    $msg = updateFilm($_POST, $film['id_film'], $film['picture_film']);
    
    if(!empty($msg))
        $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $msg, 'alert' );
    else
        $msg = KTMakeDiv('SUCCESS', 'alert alert-success el_top40 text-center wBold', T_("La fiche du Film a été modifiée"), 'success' );
        
        // Rechargement de l'article modifié
        $film = getFicheFilm($idfilm);
        
// Sinon alors suppression        
}elseif(!empty($_POST)){
   
    $msg = deleteItem('film', $_POST['id_film']);
    
    if(!empty($msg['msg'])) {
        if($msg['status'])
            $msg = KTMakeDiv('SUCCESS', 'alert alert-success el_top40 text-center wBold', $msg['msg'], 'success' );
        else
            $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $msg['msg'], 'alert' );
    }else
        $msg = KTMakeDiv('SUCCESS', 'alert alert-success el_top40 text-center wBold', T_("La fiche du Film a été modifiée"), 'success' );
    
    $film = getEmptyFilm();
    //print_r($film); die();
    
    // Renvoi vers la page listing après 2 secondes
    header('Refresh: 2; url= '.K_PTH_MODS.D_PRIMARY.DS.'list'); 
     
}


// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
    $engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_PRIMARY . DS );

// Assignation du template
    $engine->set_file( D_PRIMARY, 'tpl_updfilm.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Afficher le texte d'introduction
    $engine->set_var('txt_welcom', $GLOBALS['G_TXT_WELCOM']); 

// Afficher un message si non vide
    if(!empty($msg)) $engine->set_var('message', $msg);

// Locales Javascript
    $engine->set_var('locales-js', includeLocalesJS('updfilm'));
    
// Modale confirmation suppression
    $engine->set_var('modal-delete-confirm', modalDeleteFilmConfirm($film['titre_film']));    

// Données du film
    $engine->set_var('titre_film', $film['titre_film']); 
    $engine->set_var('reference_film', $film['reference_film']); 
    $engine->set_var('annee_prod_film', $film['date_prod_film']); 
    $engine->set_var('resume_film', $film['resume_film']); 
    $engine->set_var('lien_film', $film['lien_film']);
    $engine->set_var('acteurs_film', $film['acteur_film']);
    $engine->set_var('duree_film', $film['duree_film']);
    $engine->set_var('commentaire_film', $film['commentaire_film']);
    $engine->set_var('auteur_film', $film['auteur_film']);
    $engine->set_var('avis_film', $film['avis_film']);
    $engine->set_var('poster_film', $film['picture_film']);
    $engine->set_var('id_film', $film['id_film']);

// Poster / Affiche
    if(!empty($film["picture_film"])) {
       // Lien de l'affiche    
       $engine->set_var('picture_film',  buildLink(D_PRIVATE . DS . D_DATAS . DS . D_IMG . DS . D_THUMB . DS . $film["picture_film"]));
       $engine->set_var('btn-afficher-poster', '<button type="button" class="btn btn-xs btn-primary" data-toggle="popover">Afficher</button>');
       $engine->set_var('condition-del-poster', '<button type="button" class="btn btn-xs btn-danger" id="active_del_poster" name="active_del_poster">{trm-activer-suppression}</button> <input class="" type="checkbox" id="del_picture" name="del_picture" title="{trm-supprimer}"><label id="lbl_message_del_picture"></label>');
    }else{
       $engine->set_var('btn-afficher-poster', '');
       $engine->set_var('btn-afficher-poster', '<p>'.T_("Actuellement, ce Film ne possède pas d'affiche").'</p>');  
    }
    $engine->set_var('trm-affiche-a-supprimer', T_("L\'affiche actuelle sera supprimée lors de la modification"));

// Listes déroulantes
    $engine->set_var('select_categories', HTMLselectCategories($film['categorie_id'])); 
    $engine->set_var('select_supports', HTMLselectSupports($film['support_id'])); 
    
// Boutons options 
    $engine->set_var('btnoptions_cote', HTMLoptionCote($film['avis_film'])); 
    $engine->set_var('btnoptions_visionne', HTMLoptionVsion($film['vision_film']));
    
    $engine->set_var('url-fiche-film', '?id='.$film['id_film']);
    
// Inclusion des constantes et variables communes
    include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';
    
// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{
    // DEBUG MODE ON FIREPHP
    $firephp = FirePHP::getInstance(K_DEBUG);  
    if(isset($firephp)) $firephp->setEnabled(K_DEBUG);
    $firephp->dump('SESSION', $_SESSION);   
} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_PRIMARY );

// Rendu du template
$engine->p( 'display' );

