<?php

//-----------------------------------------------------------------------------
// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++    

$HTMLsearchListe = null;
 
if(!empty($_POST['categorie_id'])) {
    
    // Récupération de l'ID de la catégorie reçue par $_POST
    $categorie_id = $_POST['categorie_id'];   
    
    // Demande le nom de la catégorie pour affichage message
    $nomCategorie = getCategorieByID($categorie_id);
    $nomCategorie = $nomCategorie['titre_categorie'];
    
    // Demander la liste des Films qui sont dans la catégorie passée paramètre
    $search_liste = getListFilmByCategorie($categorie_id);
    

    if(!$search_liste) {
         $msg = T_("Il n'y a pas encore de Film dans la catégorie : <span class=\"wBlueColorLight\">$nomCategorie</span>.<br> Veuillez en sélectionner une autre.");
         $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $msg, 'alert' );
    }else{
        $HTMLsearchListe .='<h4>'.T_("Résultat(s)").'</h4>';
        $HTMLsearchListe .= '<ul class="search_liste">';
        foreach($search_liste as $key => $value) {
           $HTMLsearchListe .= '<li class="li_search_list"><a href="fiche?id='.$value['id'].'" class="a_search_list"><div class="div_search"><span class="search_titre">'.$value['titre'].'</span> <span class="search_ref">'.$value['reference'].'</span></div></a></li>';
        }
        $HTMLsearchListe .= '</ul>';        
    }
}elseif(isset($_POST['searchString'])){
    $msg = T_("Veuillez sélectionner une catégorie");
    $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $msg, 'alert' );
}

// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_PRIMARY . DS );

// Assignation du template
$engine->set_file( D_PRIMARY, 'tpl_searchbycat.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Afficher le texte d'introduction
$engine->set_var('txt_welcom', $GLOBALS['G_TXT_WELCOM']); 

// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Afficher le titre                          
$engine->set_var('titre', K_TITLE);

// Description
$engine->set_var('description', K_DESCRIPTION);

// Affichage des catégories
$engine->set_var('liste_btn_categorie',HTMLBtnListCategories());

// Affichage des résultats de la recherche par catégorie
$engine->set_var('search_liste', $HTMLsearchListe);



// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{
    // DEBUG MODE ON FIREPHP
    $firephp = FirePHP::getInstance(K_DEBUG);  
    if(isset($firephp)) $firephp->setEnabled(K_DEBUG);
    $firephp->dump('SESSION', $_SESSION );   
} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_PRIMARY );

// Rendu du template
$engine->p( 'display' );