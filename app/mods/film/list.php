<?php

//-----------------------------------------------------------------------------
// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++    

// Vérifie si job reçoit une demande
$job = null;
if (isset($_GET['job'])) {

    $job = $_GET['job'];
    if ($job == 'get_listfilms') {
        // Chargement du contrôleur
        echo getListFilms();
        exit();
    }
}

// Bouton ajouter film
if(isset($_SESSION['su']) && $_SESSION['su'] == 1 ) {
    $btnSousMenu = '
        <div class="pull-right mBot20">
            <a class="btn btn-success btn-xs mBotStick wMgLeft10" href="{url-admin-addfilm}"><i class="fa fa-plus-square-o" aria-hidden="true"></i> {titre-addfilm}</a>
            <a class="btn btn-primary btn-xs mBotStick wMgLeft10" href="{url-main-index}"><i class="fa fa-search" aria-hidden="true"></i> {trm-rechercher}</a>
        </div>
            . '; 
}else {
    $btnSousMenu = '
        <div class="pull-right mBot20">
            <a class="btn btn-primary btn-xs mBotStick wMgLeft10" href="{url-main-index}"><i class="fa fa-search" aria-hidden="true"></i> {trm-rechercher}</a>
        </div>
            . ';     
}

// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_PRIMARY . DS );

// Assignation du template
$engine->set_file( D_PRIMARY, 'tpl_list.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Afficher le texte d'introduction
$engine->set_var('txt_welcom', $GLOBALS['G_TXT_WELCOM']); 

// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Afficher le titre                          
$engine->set_var('titre', K_TITLE);

// Description
$engine->set_var('description', K_DESCRIPTION);

// Titre
$engine->set_var('trm-liste-films', T_("Liste des Films"));

//Bouton(s) du sous-menu
$engine->set_var('btn-smenu', $btnSousMenu);

// Ajoute les css pour la datatable
$engine->set_var('datatables-css', insertCSSDatatables(buildLink(K_PTH_THM)));
// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// Chargement du code JS du module DataTables
$engine->set_var('footer-js-datatables', insertFooterJsDatatables(buildLink(K_PTH_THM)));

// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{
    // DEBUG MODE ON FIREPHP
    $firephp = FirePHP::getInstance(K_DEBUG);  
    if(isset($firephp)) $firephp->setEnabled(K_DEBUG);
    $firephp->dump('SESSION', $_SESSION );   
} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_PRIMARY );

// Rendu du template
$engine->p( 'display' );

