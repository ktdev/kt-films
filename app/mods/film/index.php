<?php

//-----------------------------------------------------------------------------
// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++    

$HTMLsearchListe = null;

if(getNbTotFilms() > 0)
    $nbFilms = getNbTotFilms();
else 
    $nbFilms = 0;     

   

// Traitement de la reherche
if(!empty($_POST['searchString'])) {
    
    $searchString = inputSecurity($_POST['searchString']);
    $search_liste = searchFilm('titre_film', $searchString);
    if(!$search_liste) {
         $msg = T_("La recherche ne retourne pas de résultat(s), essayez à nouveau.");
         $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $msg, 'alert' );
    }else{
        $HTMLsearchListe .='<h4>'.T_("Résultat(s)").'</h4>';
        $HTMLsearchListe .= '<ul class="search_liste">';
        foreach($search_liste as $key => $value) {
           $HTMLsearchListe .= '<li class="li_search_list"><a href="fiche?id='.$value['id'].'" class="a_search_list"><div class="div_search"><span class="search_titre">'.$value['titre'].'</span> <span class="search_ref">'.$value['reference'].'</span></div></a></li>';
        }
        $HTMLsearchListe .= '</ul>';        
    }
}elseif(isset($_POST['searchString'])){
    $msg = T_("Veuillez entrer un mot/terme dans le champ de recherche");
    $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $msg, 'alert' );
}

// Traitement des boutons du sous-menu
if(isset($_SESSION['su']) && $_SESSION['su'] == 1 ) {
    $btnSousMenu = '
        <div class="pull-right">
            <a class="btn btn-success btn-xs mBotStick wMgLeft10 pull-right" href="{url-admin-addfilm}"><i class="fa fa-plus-square-o" aria-hidden="true"></i> {titre-addfilm}</a>
        </div>
        <div class="el_top60"> </div>
        '; 
}else
    $btnSousMenu = '';

// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_PRIMARY . DS );

// Assignation du template
$engine->set_file( D_PRIMARY, 'tpl_index.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Afficher le texte d'introduction
$engine->set_var('txt_welcom', $GLOBALS['G_TXT_WELCOM']); 

// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Afficher le titre                          
$engine->set_var('titre', K_TITLE);

// Description
$engine->set_var('description', K_DESCRIPTION);

// Affichage des résultats de la recherche
$engine->set_var('search_liste', $HTMLsearchListe);

// Bouton(s) du sous-menu
$engine->set_var('btn-smenu', $btnSousMenu);

// Nombre de Films total
$engine->set_var('nb-total-films', $nbFilms);

// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{
    // DEBUG MODE ON FIREPHP
    $firephp = FirePHP::getInstance(K_DEBUG);  
    if(isset($firephp)) $firephp->setEnabled(K_DEBUG);
    $firephp->dump('SESSION', $_SESSION );   
} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_PRIMARY );

// Rendu du template
$engine->p( 'display' );

