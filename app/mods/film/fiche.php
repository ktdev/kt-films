<?php

//-----------------------------------------------------------------------------
// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++    

$idfilm = $_GET["id"];
$film = getFicheFilm($idfilm);

//print_r ($film); die();

$link_edit = buildLink(D_APP . DS . D_MODS . DS . D_PRIMARY . DS . 'updfilm?id='.$idfilm);

if($_SESSION['pseudo'] == 'admin') {
    $button_edit = '<a class="btn btn-success btn-xs mBotStick wMgLeft10" href="'.$link_edit.'"><i class="fa fa-pencil" aria-hidden="true"></i> {trm-modifier}</a>';
}else
    $button_edit = '';

//DEBUG//print_r($film); die();

// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_PRIMARY . DS );

// Assignation du template
$engine->set_file( D_PRIMARY, 'tpl_fiche.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Afficher le texte d'introduction
$engine->set_var('txt_welcom', $GLOBALS['G_TXT_WELCOM']); 

// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

$engine->set_var('titre_film', $film["titre_film"]);
$engine->set_var('titre_categorie', $film["titre_categorie"]);
$engine->set_var('titre_support', $film["titre_support"]);
$engine->set_var('annee_prod', $film["date_prod_film"]);
$engine->set_var('lien_film', $film["lien_film"]);
$engine->set_var('acteurs_film', $film["acteur_film"]);
$engine->set_var('resume_film', html_entity_decode(nl2br($film["resume_film"])));
$engine->set_var('reference_film', $film["reference_film"]);
$engine->set_var('duree_film', $film["duree_film"]);
$engine->set_var('commentaire_film', $film["commentaire_film"]);
$engine->set_var('realisateur_film', $film["auteur_film"]);
$engine->set_var('visionne_film', ($film["vision_film"] == 1)? '{trm-positif}' : '{trm-negatif}' );
$engine->set_var('button-edit', $button_edit);

// Afficher le titre                          
$engine->set_var('titre', K_TITLE);

// Description
$engine->set_var('description', K_DESCRIPTION);

// Traitement de l'affiche
    $nopict =   buildLink(D_THM . DS . D_THM_USE . DS . 'images' . DS . 'others' . DS . 'no-pict.jpg');

    // Si le champ picture_film contient des données et si ces données correspondent à un fichier on affiche l'image
    if (!empty($film["picture_film"] && file_exists(ABSPATH.DS.D_PRIVATE . DS . D_DATAS . DS . D_IMG . DS . D_THUMB . DS . $film["picture_film"]))) {

        // Traitement de l'affiche miniature
        $large_picture = buildLink(D_PRIVATE . DS . D_DATAS . DS . D_IMG . DS . D_ORIGINAL . DS . $film["picture_film"]);
        $engine->set_var ('affiche_film', '<a class="swipebox" href="'.$large_picture.'" title="'.$film["picture_film"].'"><img class="poster-border img-responsive displayed" src="{picture_film}" alt=" Nom du fichier : {nom_fichier_poster}" /></a>');

    }else
        $engine->set_var ('affiche_film', '<img class="poster-border img-responsive displayed no-pict" src="'.$nopict.'" alt="{trm-no-poster}" />');

    $engine->set_var('picture_film',  buildLink(D_PRIVATE . DS . D_DATAS . DS . D_IMG . DS . D_THUMB . DS . $film["picture_film"]));
    $engine->set_var('nom_fichier_poster', $film["picture_film"]);
    
// Traitement de la StarsBars des Avis
    if($film["avis_film"] >= 1)
        $engine->set_var('stars-bar', HTMLStarsBarCote($film["avis_film"]));

// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{
    // DEBUG MODE ON FIREPHP
    $firephp = FirePHP::getInstance(K_DEBUG);  
    if(isset($firephp)) $firephp->setEnabled(K_DEBUG);
    $firephp->dump('SESSION', $_SESSION );   
} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_PRIMARY );

// Rendu du template
$engine->p( 'display' );

