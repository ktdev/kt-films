<?php


//-----------------------------------------------------------------------------
// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++  

if(!empty($_POST)){
    
    //DEBUG//print_r($_POST);
    $result = addFilm($_POST);
   
    // Si tout est OK
    if($result['status'])
         $msg = KTMakeDiv('SUCCESS', 'alert alert-success el_top40 text-center wBold', $result['msg'], 'success' );
    else
         $msg = KTMakeDiv('ALERT', 'alert alert-danger el_top40 text-center wBold', $result['msg'], 'alert' ); 
        
}

// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_PRIMARY . DS );

// Assignation du template
$engine->set_file( D_PRIMARY, 'tpl_addfilm.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Afficher le texte d'introduction
$engine->set_var('txt_welcom', $GLOBALS['G_TXT_WELCOM']); 

// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Listes déroulantes
    $engine->set_var('select_categories', HTMLselectCategories()); 
    $engine->set_var('select_supports', HTMLselectSupports()); 
    
// Boutons option
    $engine->set_var('btnoptions_cote', HTMLoptionCote()); 
    $engine->set_var('btnoptions_visionne', HTMLoptionVsion());


// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{
    // DEBUG MODE ON FIREPHP
    $firephp = FirePHP::getInstance(K_DEBUG);  
    if(isset($firephp)) $firephp->setEnabled(K_DEBUG);
    $firephp->dump('SESSION', $_SESSION);   
} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_PRIMARY );

// Rendu du template
$engine->p( 'display' );

