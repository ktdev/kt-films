<?php

// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++
$job = null;
if (isset($_GET['job'])) {

    $job = $_GET['job'];
    if ($job == 'mystore') {
        
        // Code fictif pour l'exemple
        echo '<h2>Ceci est un exemple</h2> Vous venez d\'exécuter votre store !<br>';
        
        $cpt = null;
        $cpt = @$_GET['cpt']; 
        
        if(!isset($cpt))
           $cpt = 1;
        else
           $cpt = $_GET['cpt']; 
        
        // Condition sur le compteur : 3 itérations
        if($cpt >= 4){
            echo 'Game Over ;)';
            exit();
        }else{
            echo $cpt;
            $cpt++;
            // Refresh de la page avec le compteur en paramètre
            header('Refresh: 1; teststore?job=mystore&cpt='.$cpt);
        }
        
        
            
        
        
        // Sortie du script pour garder l'intégrité des données du store
        exit();
    }
}

// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++
// Instanciation du moteur de template
$engine = new Template(ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_TESTING . DS);

// Assignation du template
$engine->set_file(D_TESTING, 'tpl_teststore.htm');

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Afficher le texte d'introduction
$engine->set_var('txt_welcom', $GLOBALS['G_TXT_WELCOM']);

// Afficher un message si non vide
if (!empty($msg))
    $engine->set_var('message', $msg);

// Afficher le titre
$engine->set_var('titre', 'MODULE DE TEST DES STORES');

// Stores à tester
$engine->set_var('stores-a-tester', '
    <li><a href="teststore?job=mystore">EXEMPLE : Lancer l\'exécution de votre store</a></li>
');

// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++
// Section de débugage de la page
if (K_DEBUG) {
    // DEBUG MODE ON FIREPHP
    $firephp = FirePHP::getInstance(K_DEBUG);
    if (isset($firephp))
        $firephp->setEnabled(K_DEBUG);
    $firephp->dump('SESSION', $_SESSION);
}
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse('display', D_TESTING);

// Rendu du template
$engine->p('display');
