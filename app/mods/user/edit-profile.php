<?php

//-----------------------------------------------------------------------------
// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++    

if(isset($_POST['edit']))
{
     // Initialisations ////////////////////////////////////////////////////////
    $result = array();
    $saveDatas = $_POST;
    
    $stopProcess = 0;    
    $result['password'] = 0;
    $result['passwordConfirm'] = 0;
    $result['passwordConcordence'] = 0;
    
    // Verify datas form //////////////////////////////////////////////////////    
        
        //DEBUG//echo'<pre>'; print_r($_POST); //die(); echo'</pre>';
        
        // Test Sequences 1 / 2 / 3
        if(empty($_POST['password'])) {
            $result['password'] = 1;
            $stopProcess = 1;
        }
        
        if(empty($_POST['passwordConfirm'])) {
            $result['passwordConfirm'] = 1;
            $stopProcess = 1;
        }
        
        if($_POST['passwordConfirm'] != $_POST['password']) {
            $result['passwordConcordence'] = 1;
            $stopProcess = 1;
        }else{
            $password = $_POST['password'];
            $passwordConfirm = $_POST['passwordConfirm'];
        }
        
         // Si le formulaire est correcte //////////////////////////////////////        
        if(!$stopProcess) 
        {
            // Initialisations
            $stringMsg = '';
            $userid = $_POST['userid'];
            $hashPassword = null;
            
            $hashPassword = KTHashPasswd($password, 10);
            
            try {               
                
                // Modification du mot de passe
                if($hashPassword)
                    $result = KTChangePassword($userid, $hashPassword);
                else{
                    $result['msg'] = T_('Erreur lors du cryptage du mot de passe');
                }
                                
            }catch (Exception $e) {                           
                $exception = $e->getMessage();
                $result = $e->getCode();                                  
            }
            
            // Réponse en fonction du résultat
            if ($result['request'])
            {
                $msg = KTMakeDiv('SUCCESS', 'alert alert-success text-center', T_('Votre mot de passe a été modifié'), 'success'); 
               
            }else{
                $txt =  $result['msg'];                
                $msg = KTMakeDiv('ALERT', 'alert alert-danger text-center', $txt, 'alert');
            }
            
        }else{
            $msg = "";
            if($result['password'])
                $msg = "<p>".T_("Le champs nouveau mot de passe est obligatoire")."</p>";
            if($result['passwordConfirm'])
                $msg .= "<p>".T_("Le champs confirmation est obligatoire")."</p>";  
            if($result['passwordConcordence'])        
                $msg .= "<p>".T_("Les champs ne correspondent pas")."</p>"; 

            $msg = KTMakeDiv('ALERT', 'alert alert-danger text-center', $msg, 'alert');
        }
        
}


// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_USER . DS );

// Assignation du template
$engine->set_file( D_USER, 'tpl_edit-profile.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Afficher le texte d'introduction
$engine->set_var('txt_welcom', $GLOBALS['G_TXT_WELCOM']); 

// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Variables et termes à afficher
$engine->set_var('trm-editer-profil', T_('Modifier votre profil'));  
$engine->set_var('trm-modifier-motdepasse', T_('Changer votre mot de passe'));
$engine->set_var('trm-modifier-email', T_('Changer votre adresse e-mail'));
$engine->set_var('trm-new-motdepasse', T_('Nouveau mot de passe'));
$engine->set_var('trm-votre-email-actuel', T_('Votre adresse e-mail actuelle est: '));
$engine->set_var('trm-new-email', T_('Nouvel e-mail'));
$engine->set_var('trm-modifier-par-admin', T_('Votre adresse e-mail ne peut-être mise à jour que par un administrateur'));

// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{
    // DEBUG MODE ON FIREPHP
    $firephp = FirePHP::getInstance(K_DEBUG);  
    if(isset($firephp)) $firephp->setEnabled(K_DEBUG);
    $firephp->dump('SESSION', $_SESSION);   
} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_USER );

// Rendu du template
$engine->p( 'display' );
