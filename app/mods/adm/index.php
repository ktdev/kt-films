<?php


//-----------------------------------------------------------------------------
// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++    

// Traitement du nombre de Films total
if(getNbTotFilms() > 0)
    $nbFilms = getNbTotFilms();
else 
    $nbFilms = 0;   

// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_ADM . DS );

// Assignation du template
$engine->set_file( D_ADM, 'tpl_index.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Afficher le texte d'introduction
$engine->set_var('txt_welcom', $GLOBALS['G_TXT_WELCOM']); 

// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Variables et termes à afficher
$engine->set_var('value-nbr-objets', 0);
$engine->set_var('titre-administration', T_('Tableau de bord'));
$engine->set_var('trm-mon-titre', T_('Gérer les films'));
$engine->set_var('trm-ajouter-objet', T_('Ajouter un film'));
$engine->set_var('trm-lister-gerer-objets', T_('Lister / Editer les films'));

// Nombre de Films total
$engine->set_var('nb-total-films', $nbFilms);


// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{
    // DEBUG MODE ON FIREPHP
    $firephp = FirePHP::getInstance(K_DEBUG);  
    if(isset($firephp)) $firephp->setEnabled(K_DEBUG);
    $firephp->dump('SESSION', $_SESSION);   
} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_ADM );

// Rendu du template
$engine->p( 'display' );

