<?php


//-----------------------------------------------------------------------------

// +++  +++ +++ +++ +++ +++  +++ CONTROLER SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++    

$timeTarget = 0.05; // 50 millisecondes

// Détermine la valeur de cost la plus appropriée
$cost = 8;
do {
    $cost++;
    $start = microtime(true);
    password_hash("test", PASSWORD_BCRYPT, ["cost" => $cost]);
    $end = microtime(true);
} while (($end - $start) < $timeTarget);


$options = [
    'cost' => $cost
];
$passToHash = 'demo123';
$passHashed = password_hash($passToHash, PASSWORD_BCRYPT, $options);

// +++  +++ +++ +++ +++ +++  +++ TEMPLATE SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Instanciation du moteur de template
$engine = new Template( ABSPATH . D_THEMES . DS . D_THM_USE . DS . D_TPL . DS . D_ADM . DS );

// Assignation du template
$engine->set_file( D_ADM, 'tpl_settings.htm' );

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Afficher le texte d'introduction
$engine->set_var('txt_welcom', $GLOBALS['G_TXT_WELCOM']); 

// Afficher un message si non vide
if(!empty($msg)) $engine->set_var('message', $msg);

// Variables et termes à afficher
$engine->set_var('titre-site-parametres', T_('Paramètrage'));
$engine->set_var('titre-parametre', T_('Paramètres'));

$engine->set_var('trm-valeur-cost', T_('Valeur actuelle du "cost"'));
$engine->set_var('trm-conseillee', T_('Conseillée'));
$engine->set_var('trm-hachage', T_('Hachage'));
$engine->set_var('trm-nom-parametre', T_('Nom du paramètre'));
$engine->set_var('trm-valeur-parametre', T_('Valeur du paramètre'));
$engine->set_var('trm-empreinte-numerique', T_('Empreinte numérique'));
$engine->set_var('trm-exemple', T_('Exemple'));
$engine->set_var('trm-motdepasse',$passToHash);
                                               
$engine->set_var('value-cost-conseillee', $cost);
$engine->set_var('value-cost-actuel', COST_HASH);
$engine->set_var('value-mdp-hash',$passHashed);

// Inclusion des constantes et variables communes
include ABSPATH . DS . D_CORE . DS . 'defined.common.inc.php';

// +++  +++ +++ +++ +++ +++  +++ DEBUG SECTION +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ 

// Section de débugage de la page
if(K_DEBUG)
{
    // DEBUG MODE ON FIREPHP
    $firephp = FirePHP::getInstance(K_DEBUG);  
    if(isset($firephp)) $firephp->setEnabled(K_DEBUG);
    $firephp->dump('SESSION', $_SESSION);   
} 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Remplacement des variables du template par les valeurs associées
$engine->parse( 'display', D_ADM );

// Rendu du template
$engine->p( 'display' );

