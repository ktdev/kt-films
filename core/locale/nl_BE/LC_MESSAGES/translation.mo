��    B      ,  Y   <      �     �     �     �     �     �     �            !   &     H     _     l     s     �  '   �     �  	   �  r   �     A     I     U     d     l  &   s  .   �     �     �     �          (  "   5     X     j          �     �     �     �  (   �     �     �     	     	     "	     .	     7	     G	     N	     ^	     x	  #   �	  I   �	  X   �	     T
  H   g
     �
  #   �
     �
        %         8  #   Y     }     �     �    �     �     �     �     �     �     �       
             3     J     S     Z     g     o  	   �     �  R   �  	   �     �     �            #     )   ?     i     v     �     �  
   �     �     �     �       
        $  
   1     <     D  
   b     m     �     �  	   �  	   �  	   �     �     �  !   �     �     �  7     ?   I     �  9   �     �     �     �  	   
          2     F     X     \     _     /      "   )               7   #   6             1   (   
   '   8                                    -   .      ,   $             =   :                    A              5   +       2   B      4   >   *                       9                     	       <   %   !      ;       @             3      &       ?       0        Accéder au module par défaut Aide Anglais Bonjour Changer votre adresse e-mail Changer votre mot de passe Confirmation Conseillée Contenu en cours de rédaction... Créer un nouvel objet Déconnexion E-mail Empreinte numérique Enregistrer Erreur lors du cryptage du mot de passe Exemple Français Félicitation ! Il ne vous reste plus qu'a créer un contrôleur sur votre module principal ainsi que son template Hachage Identifiant Identification Inconnu Langue Le champs confirmation est obligatoire Le champs nouveau mot de passe est obligatoire Lecture seule Les champs ne correspondent pas Lister / Editer les objets Modifier votre profil Mot de passe Mot de passe modifié avec succès Nom du paramètre Nouveau mot de passe Nouvel e-mail Néerlandais Paramètrage Paramètres Profil Quelle action souhaitez-vous réaliser ? Quitter Retour sur la page d'accueil SELECTIONNER Se connecter Se souvenir Sommaire Tableau de bord Thème Titre du module Valeur actuelle du "cost" Valeur du paramètre Votre adresse e-mail actuelle est:  Votre adresse e-mail ne peut-être mise à jour que par un administrateur Votre compte utilisateur est inactif, veuillez prendre contact avec votre administrateur Votre indentifiant Votre login et/ou votre mot de passe sont erronés - Veuillez réessayer Votre mot de passe Votre mot de passe a été modifié Votre profil utilisateur Vous déconnecter Vous êtes actuellement connecté sur Vous êtes à présent connecté Vous êtes à présent déconnecté avec est par Project-Id-Version: WAL
POT-Creation-Date: 2016-12-20 13:56+0100
PO-Revision-Date: 2016-12-20 13:59+0100
Last-Translator: Dev Team EPI <web.lantin@just.fgov.be>
Language-Team: KTDev <ken@kentaro.be>
Language: nl_BE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ../../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: _;T_
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: themes/core/js
 Toegang tot de standaard module Helpen Engels Hallo Wijzig uw e-mailadres Wijzig uw wachtwoord Bevestiging Aanbevolen Inhoud in voorbereiding... Maak een nieuwe object Afmelden E-mail Vingerafdruk Opslaan Fout wachtwoord-encryptie Voorbeeld Frans Gefeliciteerd ! Het blijft alleen een controller op uw hoofd module en de template Encryptie Inloggen identificatie Onbekend Taal De bevestiging is verplichte velden Het nieuwe wachtwoord velden is verplicht Alleen-zelen De velden komen niet overeen Objecten Lister / bewerken Bewerk je profiel Wachtwoord Wachtwoord met succes gewijzigd Parameter naam Nieuw wachtwoord Nieuwe e-mail Nederlands Instellingen Parameters Profiel Welke actie wilt u bereiken ? Vertrekken Terug naar de home page SELECT Log in Onthouden Overzicht Dashboard Thema Module titel De huidige waarde van de "kosten" Parameter value Uw huidige e-mailadres is: Uw e-mailadres kan worden bijgewerkt door een beheerder Uw account inactief is, kunt u contact opnemen met uw beheerder Uw login Uw login en / of wachtwoord onjuist - Probeer het opnieuw Uw wachtwoord Uw wachtwoord is gewijzigd Uw gebruikersprofiel Uitloggen U bent momenteel aangemeld op U bent nu verbonden U bent nu offline met is bij 