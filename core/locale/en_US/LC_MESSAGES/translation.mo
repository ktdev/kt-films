��    B      ,  Y   <      �     �     �     �     �     �     �            !   &     H     _     l     s     �  '   �     �  	   �  r   �     A     I     U     d     l  &   s  .   �     �     �     �          (  "   5     X     j          �     �     �     �  (   �     �     �     	     	     "	     .	     7	     G	     N	     ^	     x	  #   �	  I   �	  X   �	     T
  H   g
     �
  #   �
     �
        %         8  #   Y     }     �     �    �     �     �     �     �     �     �     �               +     ;     D     K     Z     a     {     �  ^   �     �     �     �                 #   6  	   Z     d     x     �     �     �     �     �  	   �     �  
   �     �       "        .     3     D     K     R     [  	   c     m     s     �     �     �  :   �  @     
   G  ;   R     �     �     �  
   �     �     �     
     !     &     )     /      "   )               7   #   6             1   (   
   '   8                                    -   .      ,   $             =   :                    A              5   +       2   B      4   >   *                       9                     	       <   %   !      ;       @             3      &       ?       0        Accéder au module par défaut Aide Anglais Bonjour Changer votre adresse e-mail Changer votre mot de passe Confirmation Conseillée Contenu en cours de rédaction... Créer un nouvel objet Déconnexion E-mail Empreinte numérique Enregistrer Erreur lors du cryptage du mot de passe Exemple Français Félicitation ! Il ne vous reste plus qu'a créer un contrôleur sur votre module principal ainsi que son template Hachage Identifiant Identification Inconnu Langue Le champs confirmation est obligatoire Le champs nouveau mot de passe est obligatoire Lecture seule Les champs ne correspondent pas Lister / Editer les objets Modifier votre profil Mot de passe Mot de passe modifié avec succès Nom du paramètre Nouveau mot de passe Nouvel e-mail Néerlandais Paramètrage Paramètres Profil Quelle action souhaitez-vous réaliser ? Quitter Retour sur la page d'accueil SELECTIONNER Se connecter Se souvenir Sommaire Tableau de bord Thème Titre du module Valeur actuelle du "cost" Valeur du paramètre Votre adresse e-mail actuelle est:  Votre adresse e-mail ne peut-être mise à jour que par un administrateur Votre compte utilisateur est inactif, veuillez prendre contact avec votre administrateur Votre indentifiant Votre login et/ou votre mot de passe sont erronés - Veuillez réessayer Votre mot de passe Votre mot de passe a été modifié Votre profil utilisateur Vous déconnecter Vous êtes actuellement connecté sur Vous êtes à présent connecté Vous êtes à présent déconnecté avec est par Project-Id-Version: WAL
POT-Creation-Date: 2016-12-20 13:58+0100
PO-Revision-Date: 2016-12-20 13:59+0100
Last-Translator: Dev Team EPI <web.lantin@just.fgov.be>
Language-Team: KTDev <ken@kentaro.be>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ../../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: _;T_
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: themes/core/js
 Access the default module Help English Hello Change your email address Change your password Confirm Recommended Content being written... Create New Item Sign Out E-mail Message digest Record Encryption password error Sample French Congratulations ! It remains only you create a controller on your main module and its template Haschage Login Identification Unknown Language The confirm field is required. The field new password is mandatory Read only Fields do not match List / edit objects Edit your profile Password Password changed successfully Setting name New password New email Dutch Setting up Settings Profile What action do you want to perform Quit Back to homepage SELECT Log in Remember Summary Dashboard Theme Module title Current value of the "cost" Parameter value Your current email address is: Your e-mail address may not be updated by an administrator Your user account is inactive, please contact your administrator Your login Your login and/or password are incorrect - Please try again Your password Your password has been changed Your user profile Disconnect You are currently logged on You are now connected You are now logged out with is by 