<?php


//-----------------------------------------------------------------------------
// Chargement des classes et fonctions génériques
require_once ABSPATH . D_CORE . DS . D_LIBS . DS . 'firephpcore' . DS . 'firephp.class.php';
require_once ABSPATH . D_CORE . DS . D_CLAS . DS . 'session.class.php';
require_once ABSPATH . D_CORE . DS . 'session.start.php';
require_once ABSPATH . D_CORE . DS . D_CLAS . DS . 'debug.class.php';
require_once ABSPATH . D_CORE . DS . 'localization.inc.php';
require_once ABSPATH . D_PRIVATE . DS . 'conf.db.class.php';
require_once ABSPATH . D_CORE . DS . D_FCT . DS . 'functions.common.inc.php';
require_once ABSPATH . D_CORE . DS . D_FCT . DS . 'functions.db.inc.php';
require_once ABSPATH . D_CORE . DS . D_CLAS . DS . 'template.class.php';
require_once ABSPATH . D_PRIVATE . DS . 'conf.router.class.php';
require_once ABSPATH . D_CORE . DS . D_CLAS . DS . 'router.class.php';
require_once ABSPATH . D_CORE . DS . D_CLAS . DS . 'model.class.php';
require_once ABSPATH . D_CORE . DS . D_FCT . DS . 'functions-thm.inc.php';
require_once ABSPATH . D_CORE . DS . D_FCT . DS . 'functions-forms.inc.php';
require_once ABSPATH . D_CORE . DS . D_FCT . DS . 'functions-kt-films.inc.php';
require_once ABSPATH . D_CORE . DS . D_CLAS . DS . 'simpleimage.class.php';


