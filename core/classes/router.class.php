<?php

//-----------------------------------------------------------------------------

class Router extends ConfRouter
{
    private $page = K_DEF_INDEX;
    private $paramsUrl = NULL;

    public function __construct( $url, $mode, $query = NULL )
    {
        // Explode Url
        $params = explode( "/", $url );
        // Lancement de la fonction de routage
        self::KTRouter( $params, $mode, $query );
    }

    /**
    * La fonction de routage principale
    * Vérifie et découpe les urls et dirige vers la bonne ressource
    *
    * @param array $params : url 
    * @param constante $mode : M_REDIR = redirection, M_INCL = include
    * @param string $query : paramètres passer par l'url
    */
    private function KTRouter( $params, $mode, $query )
    {

        // Si l'url envoi des paramètres ($query) on crée un tableau avec la string $a_query
        // Cette variable sera accessible dans les scripts chargé par la méthode "include" 
        if(!empty($query))
        {
            parse_str($query, $this->paramsUrl);
        }

        // Traitement du path
        if( in_array( $params [0], $this->safeApplicationDirectory ) )
        {
            if( in_array( $params [1], $this->safeModuleDirectory ) )
            {
                if( in_array( $params [2], $this->safeModulesList ) )
                {
                    if( in_array( $params [3], $this->safeViewsList ) || $params [1]  == D_STORES )
                    {
                        $this->page = $params [0] . DS . $params [1] . DS . $params [2] . DS . $params [3] . '.php';

                        $level = 0;

                        // Vérifie si l'utilisateur est bien identifié
                        if(!empty($_SESSION['IDENTIFY']))
                            $identify = $_SESSION['IDENTIFY'];
                        else{
                            $_SESSION['IDENTIFY'] = 0;
                            $identify = 0;
                        }


                        // Donner l'accès libre au(x) module(s) définit dans le fichier de configuration du router :
                        if(in_array( $params[2], $this->freeAccessModule) )
                        $level = 1;

                        switch($mode)
                        {
                            case 'REDIR':

                                if( $level > 0  || $identify > 0 )
                                    header('Location: '.K_PTH_INST . $params [0] . DS . $params [1] . DS . $params [2] . DS . $params [3] );    
                                else
                                    header('Location: '.K_PTH_INST . D_APP . DS . D_MODS . DS . D_HOME . DS . 'noaccess' ); 
                                break;

                            case 'INCL':

                                if( $level > 0  || $identify > 0 )
                                {
                                    // Vérifie si la ressource existe et traite le cas particulie des stores
                                    // Si c'est un store
                                    if($params [1]  == D_STORES) {
                                        if( file_exists($params [0] . DS . $params [1] . DS . $params [2] . DS . $params [3] ));
                                    }else{
                                        if(file_exists($params [0] . DS . $params [1] . DS . $params [2] . DS . $params [3] . '.php'))
                                            include $params [0] . DS . $params [1] . DS . $params [2] . DS . $params [3] . '.php';
                                        else{
                                            $this->page = ABSPATH . D_APP . DS . D_MODS . DS .D_MISC . DS . 'error.php';
                                            $e = 'ROOTING ERROR <br> THIS CONTROLLER : <strong>'.$params [2].DS.$params [3].'</strong>  DOES NOT EXIST';
                                            include $this->page;
                                        }
                                    } 
                                }else    
                                    include ABSPATH . D_APP . DS . D_MODS . DS . D_HOME . DS . 'noaccess.php';    
                                break;    
                        }// Switch end 


                    }else {
                        $this->page = ABSPATH . D_APP . DS . D_MODS . DS .D_MISC . DS . 'error.php';
                        $e = 'ROOTING ERROR <br> THIS PAGE : <strong>'.$params [3].'</strong> DOES NOT EXIST';
                        include $this->page;
                    }
                }else {
                    $this->page = ABSPATH . D_APP . DS . D_MODS . DS .D_MISC . DS . 'error.php';
                    $e = 'ROOTING ERROR <br> THIS MODULE : <strong>'.$params [2].'</strong> DOES NOT EXIST';
                    include $this->page;                        
                }
            }else {
                $this->page = ABSPATH . D_APP . DS . D_MODS . DS .D_MISC . DS . 'error.php';
                $e = 'ROOTING ERROR <br> THE DIRECTORY MODULE : <strong>'.$params [1].'</strong> DOES NOT EXIST';
                include $this->page;
            }
        }else {
            $this->page = ABSPATH . D_APP . DS . D_MODS . DS .D_MISC . DS . 'error.php';
            $e = 'ROOTING ERROR <br> THIS RESSOURCE DOES NOT EXIST <br>
            <STRONG>ALSO CHECK YOUR INSTALLATION PATH (PATH_INSTALL) IN  DATABASE : TABLE PARAMS </STRONG>
            ';
            include $this->page;
        }


    } // End function KTRouter

    /**
    * Cette fonction retourne le nom la page appelée
    */
    public function getCurrentPage()
    {
        return $this->page;
    }

    /**
    * Cette fonction retourne les paramètres passer dans l'url
    */
    public function getParamsUrl()
    {
        return $this->paramsUrl;
    }
}

?>

