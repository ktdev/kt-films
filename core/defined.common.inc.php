<?php

/////////////////////////////////////////////////////////////////////////////////////////////////////  
//                                         DEFAULT SET_VAR
/////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////// HEADERS FOOTERS ////////////////////////////////////////////
// Affiche la section Head du code HTML
$engine->set_var('head_section', insertHeadSection(buildLink(K_PTH_THM)));
// Afficher le footer                         
$engine->set_var('footer', insertFooter());
// Afficher la Footer JS Section
$engine->set_var('footer-js-section', insertFooterJsSection(buildLink(K_PTH_THM)));

/////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////// METAS /////////////////////////////////////////////////
// Affiche le nom de l'application
$engine->set_var('appName', K_NAME);
// Affiche l'auteur de l'application
$engine->set_var('author', K_AUTHOR);
// Affiche le site de l'auteur de l'application
$engine->set_var('author_site', K_AUTHOR_SITE);
// Affiche la date de création de l'application
$engine->set_var('creation_date', K_DATE_CREATION);
// Affiche la description de l'application
$engine->set_var('description', K_DESCRIPTION);
// Affiche les mots clefs de l'application
$engine->set_var('keywords', K_KEYWORDS);
// Indique aux webcrawlers le délais de passage
$engine->set_var('revisit_after', K_REVISIT_AFTER);
// Indique aux webcrawlers le contenu indexable
$engine->set_var('robots', K_ROBOTS);
// Affiche la Date du jour
$engine->set_var('date_now', date('d/m/Y'));

/////////////////////////////////////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////// AUTRES //////////////////////////////////////////////
// Affiche la Date du jour
$engine->set_var('version', K_VER);
// Affiche la Date du jour
$engine->set_var('name_app', K_NAME);

//////////////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////// SESSIONS //////////////////////////////////////////////
// Affiche le nom du user  (nom, prénom)
if(!empty($_SESSION['pseudo'])){
     $engine->set_var('pseudo', $_SESSION['pseudo']);
      
}

if(!empty($_SESSION['email'])){
    $engine->set_var('email', $_SESSION['email']);  
}

if(!empty($_SESSION['id_user'])){
    $engine->set_var('userid', $_SESSION['id_user']);  
}


   
////////////////////////////////////////////////////////////////////////////////////////////////////  
//////////////////////////////////////// NAVBAR LINKS //////////////////////////////////////////////
// Include de la navigation
$engine->set_var('navbar', includeNavigation('.'.DS.D_CORE.DS));
$engine->set_var('menu-user', insertMenuUser());
$engine->set_var('menu-lang', insertMenuLang($query));

$itemMenuSearch = ($_SESSION['IDENTIFY']) ? '<li><a href="{url-main-index}">{trm-rechercher}</a></li>' : '';
$engine->set_var('menu-search', $itemMenuSearch);

////////////////////////////////////////////////////////////////////////////////////////////////////  
/////////////////////////////////////////// TERMES /////////////////////////////////////////////////
$engine->set_var('trm-langue', T_('Langue'));
$engine->set_var('menu-lang-fr', T_('Français'));
$engine->set_var('menu-lang-nl', T_('Néerlandais'));
$engine->set_var('menu-lang-en', T_('Anglais'));
$engine->set_var('menu-aide', T_('Aide'));
$engine->set_var('menu-main-index', T_('Rechercher'));
$engine->set_var('menu-main-list', T_('Lister'));
$engine->set_var('menu-admin-index', T_('Tableau de bord'));
$engine->set_var('menu-admin-settings', T_('Paramètres'));
$engine->set_var('menu-admin-addfilm', T_('Ajouter un Film'));
$engine->set_var('menu-quitter', T_('Quitter'));
$engine->set_var('menu-connecter', T_('Se connecter'));
$engine->set_var('menu-profile', T_('Modifier votre profil'));
$engine->set_var('trm-est', T_('est'));
$engine->set_var('trm-avec', T_('avec'));
$engine->set_var('trm-par', T_('par'));
$engine->set_var('trm-theme', T_('Thème'));
$engine->set_var('trm-enregistrer', T_('Enregistrer'));
$engine->set_var('trm-annuler', T_('Annuler'));
$engine->set_var('trm-confirmer', T_('Confirmer'));
$engine->set_var('trm-ajouter', T_('Ajouter'));
$engine->set_var('trm-lecture-seule', T_('Lecture seule'));
$engine->set_var('trm-passwd', T_('Mot de passe'));
$engine->set_var('trm-confirmation', T_('Confirmation'));
$engine->set_var('trm-obligatoire', T_('obligatoire'));
$engine->set_var('trm-rechercher', T_('Rechercher'));
$engine->set_var('trm-parcourir', T_('Parcourir'));
$engine->set_var('trm-modifier', T_('Modifier'));
$engine->set_var('trm-supprimer', T_('Supprimer'));
$engine->set_var('trm-suppression-film', T_('Suppression du Film'));
$engine->set_var('trm-activer-suppression', T_('Activer la suppression'));
$engine->set_var('trm-annee-prod', T_('Année de production')); 
$engine->set_var('trm-annee-prod-short', T_("Année Prod"));
$engine->set_var('trm-listing', T_("Listing"));
$engine->set_var('trm-trouver-film', T_("Trouver un film"));
$engine->set_var('trm-reference', T_("Référence"));
$engine->set_var('trm-categorie', T_("Catégorie"));
$engine->set_var('trm-support', T_("Support"));
$engine->set_var('trm-duree', T_("Durée"));
$engine->set_var('trm-lien', T_("Lien"));
$engine->set_var('trm-acteurs', T_("Acteurs"));
$engine->set_var('trm-synopsis', T_("Synopsis"));
$engine->set_var('trm-categorie',T_("Catégorie"));
$engine->set_var('trm-searchbycat-film',T_("Recherche par Catégorie"));
$engine->set_var('trm-commentaire-film',T_("Commentaire"));
$engine->set_var('trm-exp-commentaire-film',T_("Commentaire / location / prêt"));
$engine->set_var('trm-realisateur-film',T_("Réalisateur"));
$engine->set_var('trm-cote-film',T_("Cote attribuée au Film"));
$engine->set_var('trm-avis',T_("Avis"));
$engine->set_var('trm-visionne',T_("Visionné"));
$engine->set_var('trm-positif',T_("Oui"));
$engine->set_var('trm-negatif',T_("Non"));


$engine->set_var('trm-titre', T_('TITRE'));
$engine->set_var('trm-titre-film', T_('Titre du Film'));
$engine->set_var('trm-titre-fiche-film', T_('Fiche du Film'));
$engine->set_var('trm-modifier-fiche-film', T_('Modifier la fiche du Film'));
$engine->set_var('trm-categorie-film', T_('Catégorie du Film'));
$engine->set_var('trm-support-film', T_('Support de stockage du Film'));
$engine->set_var('trm-resume-film', T_('Résumé du film'));
$engine->set_var('trm-reference-support', T_('Référence du support'));
$engine->set_var('trm-lien-internet-film', T_('Lien vers un site'));
$engine->set_var('trm-acteurs-film', T_('Acteurs du film'));
$engine->set_var('titre-addfilm', T_('Ajouter un Film'));
$engine->set_var('titre-modfilm', T_('Modifier la fiche'));
$engine->set_var('titre-fichefilm', T_('Fiche du Film'));
$engine->set_var('trm-poster', T_('L\'affiche du Film'));
$engine->set_var('trm-no-poster', T_('Affiche non disponible'));
$engine->set_var('trm-edit-poster', T_('Changer l\'affiche'));
$engine->set_var('trm-poster-actuel', T_('Affiche actuelle'));
$engine->set_var('trm-fiche-film', T_('Consulter la fiche du Film'));
$engine->set_var('trm-liste-complete',T_('Tous les films'));
$engine->set_var('trm-duree-film',T_('Durée'));

////////////////////////////////////////////////////////////////////////////////////////////////////  
//////////////////////////////////////////// LIENS /////////////////////////////////////////////////
$engine->set_var('url-home-index',buildLink(D_APP.DS.D_MODS.DS.D_HOME.DS.'index'));
$engine->set_var('url-default-index',buildLink(K_DEF_INDEX));
$engine->set_var('url-home-login',buildLink(D_APP.DS.D_MODS.DS.D_HOME.DS.'login'));
$engine->set_var('url-home-logout', K_PTH_MODS.D_HOME.DS.'logout');
$engine->set_var('url-user-profile', K_PTH_MODS.D_USER.DS.'profile');
$engine->set_var('url-user-edit-profile', K_PTH_MODS.D_USER.DS.'edit-profile');
$engine->set_var('url-primary-index',buildLink(D_APP.DS.D_MODS.DS.D_PRIMARY.DS.'index'));
$engine->set_var('url-help-index',buildLink(D_APP.DS.D_MODS.DS.D_HELP.DS.'index'));
$engine->set_var('url-main-index', K_PTH_MODS.D_PRIMARY.DS.'index');
$engine->set_var('url-main-list', K_PTH_MODS.D_PRIMARY.DS.'list');
$engine->set_var('url-main-searchbycat', K_PTH_MODS.D_PRIMARY.DS.'searchbycat');
$engine->set_var('url-main-fiche', K_PTH_MODS.D_PRIMARY.DS.'fiche');
$engine->set_var('url-admin-index', K_PTH_MODS.D_ADM.DS.'index');
$engine->set_var('url-admin-addfilm', K_PTH_MODS.D_PRIMARY.DS.'addfilm');
$engine->set_var('url-admin-settings', K_PTH_MODS.D_ADM.DS.'settings');
$engine->set_var('url-testing-teststore',buildLink(D_APP.DS.D_MODS.DS.D_TESTING.DS.'teststore'));

//////////////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////// LOGOS /////////////////////////////////////////////////
// Affiche le logo Large                         
$engine->set_var('logo-large', buildLink(D_THEMES . DS . D_THM_USE . DS . 'images' . DS . 'logos'. DS . 'logo-large.png'));

// Affiche le logo dans la NAVBAR                         
$engine->set_var('logo', buildLink(D_THEMES . DS . D_THM_USE . DS . 'images' . DS . 'logos'. DS . 'logo.png')); 

////////////////////////////////////////////////////////////////////////////////////////////////////   
///////////////////////////////////////// DEBUG BAR ////////////////////////////////////////////////  
//       Affiche la "Debug Footer Bar"  -  Celle ci indique que l'application est en mode "DEBUG"
//                           et affiche le temps de génération de la page 
///////////////////////////////////////////////////////////////////////////////////////////////////*/                           
if(K_DEBUG)
{
    // DEBUG MODE ON FIREPHP
    $firephp = FirePHP::getInstance(K_DEBUG);  
    if(isset($firephp)) $firephp->setEnabled(K_DEBUG);
    $dbug = new debug();
    if(isset($_SESSION['startTime'])) $engine->set_var('debug_generate', $dbug->KTGenerate($_SESSION['startTime']));
}
///////////////////////////////////////////////////////////////////////////////////////////////////*/                           