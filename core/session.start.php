<?php


//-----------------------------------------------------------------------------
// Lancement des sessions
new Session();


// Initialisations

// Départ du calcul du temps de génération de la page en mode DEBUG
$_SESSION['startTime'] = microtime( true );

// Traitement de la langue
$getLocale = NULL;

if(isset($_GET['locale'])) {
    $_SESSION['locale'] = $_GET['locale'];  
}else {
    if(!isset($_SESSION['locale'])) {
        $_SESSION['locale'] = DEF_LOCALE;    
    }
}
