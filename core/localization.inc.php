<?php

//-----------------------------------------------------------------------------

// Define constants
define('PROJECT_DIR', realpath('./'));
define('LOCALE_DIR', PROJECT_DIR.DS.D_CORE.DS.'locale');
define('DEFAULT_LOCALE', DEF_LOCALE); 

// Include gettext lib
require_once D_LIBS.DS.'gettext'.DS.'gettext.inc';

$supported_locales = array('en_US', 'fr_BE', 'nl_BE');
$encoding = 'UTF-8';

//$locale = (isset($_GET['lang']))? $_GET['lang'] : DEFAULT_LOCALE;
$locale = (isset($_SESSION['locale']))? $_SESSION['locale'] : DEF_LOCALE;

// gettext setup
T_setlocale(LC_MESSAGES, $locale);

// Set the text domain as 'messages'
$domain = 'translation';
T_bindtextdomain($domain, LOCALE_DIR);
T_bind_textdomain_codeset($domain, $encoding);
T_textdomain($domain);


header("Content-type: text/html; charset=$encoding");  
