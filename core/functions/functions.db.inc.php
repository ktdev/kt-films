<?php

//-----------------------------------------------------------------------------
/**
 * Cette fonction charge tous les enregistrements
 * de la table des paramètres (params)
 *
 * @return array or JSON
 */
function KTloadParams() {
    $con = new Model('params');

    $sql = ' SELECT * FROM ' . $con->table;

    if ($con) {

        $req = $con->db->query($sql);

        $list = array();
        $i = 0;

        foreach ($req as $data) {
            $list [$i] = array(
                'id' => $data ['id_params'],
                'key' => $data ['key'],
                'value' => $data ['value']
            );
            $i ++;
        }

        //$list = json_encode( $list );

        return $list;
    } else
        return false;
}

/**
 * Vérifie si le compte utilisateur est actif
 *
 * @param mixed $user
 */
function KTUserIsActif($user) {
    $con = new Model('users');

    $q = array('pseudo' => $user);
    $sql = 'SELECT actif FROM ' . $con->table . ' WHERE pseudo = :pseudo AND actif = 1';

    $req = $con->db->prepare($sql);
    $req->execute($q);
    $count = $req->rowCount($sql);

    if ($count == 1) {
        return TRUE;
    } else
        return FALSE;
}

/**
 * Fonction d'identification des utilisateurs sur l'application
 *
 * @param mixed $user
 * @param mixed $passwd
 */
function KTIdentUser($user, $passwd) {
    $con = new Model('users');

    $q = array('pseudo' => $user);
    $sql = 'SELECT hpasswd FROM ' . $con->table . ' WHERE pseudo = :pseudo';

    $req = $con->db->prepare($sql);
    $req->execute($q);

    $dbHashPasswd = $req->fetch()[0];
    $count = password_verify($passwd, $dbHashPasswd);

    $statut = NULL;

    // Si correspondance
    if ($count == true) {
        // Si actif
        $statut = KTUserIsActif($user);
        if ($statut) {
            $st['stat'] = TRUE;
            $st['msg'] = T_('Vous êtes à présent connecté');
            $st['user'] = $user;

            // +++ récupérer les autres infos du compte utilisateur
            $sql = 'SELECT * FROM ' . $con->table . ' WHERE pseudo = :pseudo';
            $req = $con->db->prepare($sql);
            $req->execute($q);

            while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
                $st['pseudo'] = $row['pseudo'];
                $st['email'] = $row['email'];
                $st['actif'] = $row['actif'];
                $st['id_user'] = $row['id_user'];
                $st['su'] = $row['su'];
            }


            return $st;
        } else {
            $st['stat'] = FALSE;
            $st['msg'] = T_('Votre compte utilisateur est inactif, veuillez prendre contact avec votre administrateur');
            return $st;
        }
    } else {
        $st['stat'] = FALSE;
        $st['msg'] = T_('Votre login et/ou votre mot de passe sont erronés - Veuillez réessayer');
        return $st;
    }
}

/**
 * Met à jour le mot de passe de l'utilisateur dans la DB
 * @param string $userid
 * @param string $passwd
 * @return array
 */
function KTChangePassword($userid, $passwd)
{
    
    try{
        
        $con = new Model('users');
        
        // Mise à jour de la table ressource
        $sql = $con->db->prepare(
        "UPDATE $con->table 
         SET hpasswd = :passwd  
         WHERE id_user = :userid"
        );
    
        $status['request'] = $sql->execute(array(
            "userid" => $userid,
            "passwd" => $passwd
        ));
        
    }catch(PDOException $e) {
         $status['msg'] = "'UPDATE error  : '.$e->getMessage()";
         $sql_upd_res->closeCursor();
         return status;
    }    
       
    if ($status['request']) 
        $status['msg'] = T_('Mot de passe modifié avec succès');   
        
    return $status;
}

/**
 * Retourne le hash du mot de passe passé en paramètre
 *
 * @param string $passwd
 * @param integer $cost
 */
function KTHashPasswd($passwd, $cost) {

    $options = [
        'cost' => $cost
    ];

    $hashPasswd = password_hash($passwd, PASSWORD_BCRYPT, $options);

    return $hashPasswd;
}

/**
 * Vérifie l'existance d'un identifiant
 * Retourne true si l'identifiant existe ou false dans le cas contraire
 *
 * @param integer $id
 * @return boolean
 */
function isLoginExist($login) {
    $store = new PrisonerStore();
    $result = $store->checkUniqueLogin($login);
    return $result;
}


