<?php

/**
 * Liste de sélection des rôles linguistiques
 * @param type $id
 * @return string
 */
function HTMLselectLangUpdate($id = null){

    $langues = array(
        "fr" => T_('Français'),
        "nl" => T_('Néerlandais'),
        "en" => T_('Anglais')
    );

    $html = '<select class="form-control" name="langue">';                                                   
    foreach($langues as $key => $value){

        if($key == $id )
            $html .= '<option value="'.$key.'" selected>'.$value.'</option>';
        else
            $html .= '<option value="'.$key.'">'.$value.'</option>';   

    }                                                               
    $html .= '</select>';    
    return $html;
}






function HTMLradioBtnStatusUpdate($id = null){
    
    if($id){
        $status1 = 'checked';
        $status2 = '';            
    }else{
        $status1 = '';
        $status2 = 'checked';          
    }
                              
    $html='
    <label class="radio-inline">
    <input type="radio" name="statut" id="radioStatusActiv" value="1" '.$status1.'> {trm-actif}
    </label>
    <label class="radio-inline">
    <input type="radio" name="statut" id="radioStatusInactiv" value="0" '.$status2.'>{trm-inactif}
    </label>
    ';                        
  
    return $html;
}