<?php


//-----------------------------------------------------------------------------
/**
* Function Installer
*
* @param string $mode : 'CTRL_INSTALL'
*/
function KTInstaller($mode) {
    // Vérifie l'existance du fichier INSTALL.txt
    switch ($mode) {
        case 'CTRL_INSTALL' :

            $installFile = 'INSTALL.txt';
            if (file_exists(ABSPATH . $installFile)) {
                $_SESSION['installFile'] = true;
                // DEBUG // echo '<p>INSTALL file exist :' . ABSPATH . $installFile.'</p>';
                return true;
            } else {
                $_SESSION['installFile'] = false;
                // DEBUG // echo '<p>INSTALL file do not exist ! :' . ABSPATH . $installFile.'</p>';
                return false;
            }
    }
}

/**
* Retourne la valeur du paramètre recherché
* @param unknown $array
* @param unknown $key
* @return unknown
*/
function KTFindParam($array, $key) {
    foreach ($array as $param) {
        if (in_array($key, $param)) {
            return $param['value'];
        }
    }

    return FALSE;
}

/**
* Retourne la valeur de la clé recherchée
*
* @param array $array
* @param string $search
*/
function KTFindKey($array, $search) {
    foreach ($array as $key => $val) {
        if ($key == $search) {
            return $val;
        }
    }

    return FALSE;
}

/**
* Petite fonction qui converti une date au format FR vers EN
*
* @param date $mydate
*/
function convertDateFrToEn($mydate) {
    $array_date_fr = explode('/', $mydate);
    $date_us = $array_date_fr[2] . "-" . $array_date_fr[1] . "-" . $array_date_fr[0];
    return $date_us;
}

/**
* Petite fonction qui converti une date au format EN vers FR
*
* @param date $mydate
*/
function convertDateEnToFr($mydate) {
    @list($annee, $mois, $jour) = explode('-', $mydate);
    return @date('d/m/Y', mktime(0, 0, 0, $mois, $jour, $annee));
}

/**
* Fonction qui produit des liens en fonction de certains paramètres
*
* @param string $link
*/
function buildLink($link) {

    // Initialisation
    $linkBuild = NULL;
    $modulePrePath = '..' . DS . '..' . DS . '..' . DS;

    // Récupére le chemin d'installation
    $listParams = KTloadParams();
    $pathInstall = KTFindParam($listParams, 'PATH_INSTALL');

    // Supprime le double slash '//'
    //$specialPath = str_replace ('//', '/', $specialPath);
    // Si l'application est installée à la racine de l'hébergement
    if ($pathInstall == '/') {

        // Si on accède à la racine de l'application, cette dernière redirige vers le module et la page par défaut
        if ($GLOBALS['URI_ROOT'] === true) {
            $linkBuild = $link;
            // Sinon on accède directement à un module et une page (via URL)
        } else {
            $linkBuild = $modulePrePath . $link;
        }
        // Sinon l'application est installée dans un sous répertoire
    } else {

        // Si on accède à la racine de l'application, cette dernière redirige vers le module et la page par défaut
        if ($GLOBALS['URI_ROOT'] === true) {
            $linkBuild = $link;
            // Sinon on accède directement à un module et une page (via URL)
        } else {
            $linkBuild = $modulePrePath . $link;
        }
    }
    return $linkBuild;
}

/**
* test si le store utilisé est autorisé
*
*  @return bool
*
*/
function storesecurity() {
    if(!empty($_SESSION['IDENTIFY']))
        return true;
    else{
        return false;
    }
}
/**
* Retourne la chaîne passée en paramètre en majuscule
* Gère correctement les caractères accentués
* 
* @param string $string
*/
function strtoupperFr($string) {

    $string = strtoupper($string);
    $string = str_replace(
        array('é', 'è', 'ê', 'ë', 'à', 'â', 'î', 'ï', 'ô', 'ù', 'û'),
        array('É', 'È', 'Ê', 'Ë', 'À', 'Â', 'Î', 'Ï', 'Ô', 'Ù', 'Û'), 
        $string 
    ); 
    return $string;

}

/**
* Fonction qui retourne les sous-répertoires d'un répertoire passer en paramètre ou false
* 
* @param string $folder
*/
function KTgetSubFolder($folder) {

    $dir = @opendir($folder);

    if($dir != false)
    {
        while ($file = readDir($dir)) {
            if (($file!=".")&&($file!="..")) {
                if (is_dir("$folder/$file")) 
                    $subFolder[] = $file;    
            }
        }
        // Close directory
        closeDir($dir);
        return $subFolder;
    }else {
        return false;    
    }

}

/**
* Génération aléatoire d'un nombre
* Par défaut sur 4 chiffres
* 
* @param integer $e
*/
function KTrands($e=4) {

    // Generation number
    $nrand = '';
    for($i=0;$i<$e;$i++)
    {
        $nrand .= mt_rand(1, 9);
    }

    // Return number.
    return $nrand;
}

/**
* Upload dans le filesystem un fichier image
* 
* @param file $file
* @return string filename
*/
function uploadPicture($file) {

    $imgName = basename($_FILES["picture-art"]["name"]);
    try {

        // Instanciation de l'objet image avec le fichier passé en paramètre
        $img = new abeautifulsite\SimpleImage($_FILES["picture-art"]["tmp_name"]);

        // Enregistre l'image dans son format original dans le sous-répertoire original
        $img->save(D_PRIVATE.DS.D_DATAS.DS.D_IMG.DS.D_ORIGINAL.DS.$imgName);

        // Enregistre l'image dans un format thumbnail dans le sous-répertoire thumb
        $img->best_fit(350, 235);
        $img->save(D_PRIVATE.DS.D_DATAS.DS.D_IMG.DS.D_THUMB.DS.$imgName);

    } catch(Exception $e) {
        return false;
    }        
    return $imgName;

}

/**
 * Fonction qui vérifie les entrées d'un input field
 * 
 * @param type $string
 * @return type
 */
function inputSecurity($string) {

    // On regarde si le type de string est un nombre entier (int)
    if(ctype_digit($string)) {
        $string = intval($string);
        
    // Pour tous les autres types    
    }else {
        $string = addcslashes($string, '%_');
    }
    
    return $string;

}