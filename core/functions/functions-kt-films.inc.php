<?php

/**
 * Retourne les chaînes de caractères de liaison entre le Javascirpt et le Php 
 * @param type $constante : updfilm
 * @return type
 */
function includeLocalesJS($constante) {

    include ABSPATH . DS . D_THEMES . DS . D_CORE . DS . D_JS . DS .'locales' . DS. 'locales.php';
    
    switch ($constante) {
        case 'updfilm':
            return LOCALES_UPDATE_FILM_JS;
            break;

    }
}
/**
 * Retourne le tableau des categories des Films
 * @return type
 */
function getCategories() {
    
    $con = new Model('categorie');
    $sql = 'SELECT * FROM ' . $con->table. ' ORDER BY titre_categorie ASC';
    $req = $con->db->query($sql);
    $arrayCategorie = $req->fetchall();
    $req->closeCursor();
    return $arrayCategorie;
}

/**
 * Retourne le tableau des categories des Films
 * @return type
 */
function getCategorieByID($id) {
    
    $con = new Model('categorie');
    $sql = "SELECT * FROM $con->table WHERE id_categorie = $id";
    $req = $con->db->query($sql);
    $nomCategorie = $req->fetch();
    $req->closeCursor();
    return $nomCategorie;
}

/**
 *  Retourne le tableau des supports des Films
 * @return type
 */
function getSupports() {
    
    $con = new Model('support');
    $sql = 'SELECT * FROM ' . $con->table. ' ORDER BY titre_support ASC';
    $req = $con->db->query($sql);
    $arraySupport = $req->fetchall();
    $req->closeCursor();
    return $arraySupport;
}

/**
 * Retourne le code HTML de liste déroulante des catégories
 * 
 * @param type $idCategorie
 * @return string
 */
function HTMLselectCategories($idCategorie = null) {

  $categories = getCategories();
 
    $html = '<select class="form-control wBlueColorLight" id="categorie" name="categorie">';  
    foreach($categories as $key => $value){

        if($value['id_categorie'] == $idCategorie )
            $html .= '<option value="'.$value['id_categorie'].'" selected>'.T_($value['titre_categorie']).'</option>';
        else
            $html .= '<option value="'.$value['id_categorie'].'">'.T_($value['titre_categorie']).'</option>';   

    }   
    $html .= '</select>';    
    return $html;
}

/**
 * Retourne le code HTML de liste déroulante des supports
 * 
 * @param type $idSupport
 * @return string
 */
function HTMLselectSupports($idSupport = null){

  $supports = getSupports();
 
    $html = '<select class="form-control wBlueColorLight" id="support" name="support">';  
    foreach($supports as $key => $value){

        if($value['id_support'] == $idSupport )
            $html .= '<option value="'.$value['id_support'].'" selected>'.T_($value['titre_support']).'</option>';
        else
            $html .= '<option value="'.$value['id_support'].'">'.T_($value['titre_support']).'</option>';   

    }   
    $html .= '</select>';    
    return $html;
}

/**
 * Retourne le code HTML des boutons d'option de la cote du Film
 * 
 * @param type $valueCote
 * @return string
 */
function HTMLoptionCote($valueCote = null){
    
    $values = array('1','2','3','4','5');
    
    $html = '';  
    foreach($values as $cote){

        if($valueCote == $cote ) {
            $html .= '
                <label class="radio-inline">
                    <input type="radio" name="cote" id="cote'.$cote.'" value="'.$cote.'" checked> '.$cote.'
                </label>
            ';
        }else{
            $html .= '
                <label class="radio-inline">
                    <input type="radio" name="cote" id="cote'.$cote.'" value="'.$cote.'"> '.$cote.'
                </label>
            ';
        }    
    }   
    
    return $html;
}

/**
 * Retourne les 2 boutons d'options pour indiquer si l'on déjà vu ou pas le Film
 * 
 * @param type $valueVision
 * @return type
 */
function HTMLoptionVsion($valueVision = null){
    
    $values = array('0','1');
    $oui = '{trm-positif}';
    $non = '{trm-negatif}';
    
    $html = '';  
    foreach($values as $vue){

        if($valueVision == $vue ) {
            $tag = ($vue == 1)? $oui : $non;
            $html .= '
                <label class="radio-inline">
                    <input type="radio" name="vision" id="vision'.$vue.'" value="'.$vue.'" checked> '.$tag.'
                </label>
            ';
        }else{
            $tag = ($vue == 1)? $oui : $non;
            $html .= '
                <label class="radio-inline">
                    <input type="radio" name="vision" id="vision'.$vue.'" value="'.$vue.'"> '.$tag.'
                </label>
            ';
        }    
    }   
    
    return $html;
}



/**
 * Retourne le code HTML de la cote du film avec des étoiles + cote
 * 
 * @param type $stars
 * @return string
 */
function HTMLStarsBarCote($stars) {
    
    $html = '';
    $starIcon =   buildLink(D_THM . DS . D_THM_USE . DS . 'images' . DS . 'others' . DS . 'star-icon.png');
    
    for($i = 1 ; $i <= $stars ; $i++)
    {
        $html .= '<img src="'.$starIcon.'" >';
    }
    
    $html .= ' <span class="wAvisFilms">'.$stars.'/5</span>'; 
    return $html;
}

/**
 * Retourne le nombre de Films de la catégorie passée en paramètre
 * 
 * @param type $idCat
 * @return int
 */
function getNbRowsByCat($idCat) {
    $con = new Model('film');
    $sql = 'SELECT * FROM ' . $con->table. ' WHERE categorie_id = '.$idCat;
    $req = $con->db->query($sql);
    $count = $req->rowCount();
    return $count;
}

/**
 * Retourne le nombre total de Films
 * 
 * @return type
 */
function getNbTotFilms() {
    $con = new Model('film');
    $sql = 'SELECT count(*) as nb FROM ' . $con->table;
    $req = $con->db->query($sql);
    $count = $req->fetch();
    return $count['nb'];
}

/**
 * Retourne le code HTML d'une liste de boutons "catégories"
 * 
 * @return string
 */
function HTMLBtnListCategories(){

   $categories = getCategories();
      
   $html = null;     
    
    foreach($categories as $key => $value) {
        $nbRows = getNbRowsByCat($value['id_categorie']);
        $html .= '<button type="submit" class="btn btn-primary btn-categorie"  name="categorie_id" value="'.$value['id_categorie'].'">'.T_($value['titre_categorie']).' <span class="badge wGreenColorLight wBadge">'.$nbRows.'</span></button>';   
    }   
     
    return $html;
}

/**
 * Retourne un json avec la liste des films de la catégorie passée en paramètre
 *
 * @return array
 */
function getListFilmByCategorie($categorie_id) {

    $con = new Model('v_liste_films_simple');
    $sql = "SELECT * FROM $con->table WHERE categorie_id = $categorie_id ORDER BY titre_film ASC";
    $req = $con->db->query($sql);
    $count = $req->rowCount();
    //echo $count; die();
    if($count > 0) {
        $arrayFilms = $req->fetchall();
        $film_liste = array();        
        foreach ($arrayFilms as $film) {

             $film_liste[] = array(
                "id" => $film['id_film'],  
                "titre" => $film['titre_film'],
                "reference" => $film['reference_film']                        
            );
        }
    }else
        $film_liste = false;

    $req->closeCursor();
    return $film_liste;
}

/**
 * Retourne un json avec la liste des films
 *
 * @return json
 */
function getListFilms() {

    $con = new Model('v_liste_films_simple');
    $sql = 'SELECT * FROM ' . $con->table . ' ORDER BY titre_film ASC';
    $req = $con->db->query($sql);
    $arrayFilms = $req->fetchall();

    foreach ($arrayFilms as $film) {

        $functions = '<div class="function_buttons"><ul>';
        $functions .= '<li class="button_info"><a href="fiche?id=' . $film['id_film'] . '"><span class="button_info">Fiche</span></a></li>';
        $functions .= '</ul></div>';
        $mysql_data[] = array(
            "titre" => $film['titre_film'],
            "categorie" => $film['titre_categorie'],
            "support" => $film['titre_support'],
            "reference" => $film['reference_film'],
            "functions" => $functions
        );
    }

    $req->closeCursor();

    // Prepare data
    $data = array(
        "data" => $mysql_data
    );

    // Convert PHP array to JSON array
    $json_data = json_encode($data);
    return $json_data;
}


/**
* Ajoute un film
* POST Array ( [titre] => aaa [categorie] => Action [support] => Disque Dur Externe [reference] => 001 [resume] => aaa a aa aaaaa a [lien] => aaa.aaa.aa ) + picture-art  
* @param array $post
*/
function addFilm($post) {

    //DEBUG//print_r($post); die();
        
    // Initialisations
    
    extract($post);
    $process = array();
    $process['status'] = true;
    $process['msg'] = NULL;  
    
    //DEBUG//   print_r($post); die();

    if(empty($titre) || empty($reference)) {
        $process['status'] = false;
        $process['msg'] = T_("Le titre et la référence sont obligatoires");
    } 

    if($process['status']) {

        // Traitement de l'image à la une
        if(isset($_FILES['picture-art']) && !empty($_FILES['picture-art']) && !empty($_FILES['picture-art']['name'])) {
            $image = uploadPicture($_FILES);
            if ($image == false){
                $image = '';
                $process['status'] = false;
                $process['msg'] = T_("Le système a rencontré un problème lors de l'enregistrement de l'image"); 
            }                  
        }else
            $image = '';
        
        /*
        // Traitement du fichier joint   
        if(isset($_FILES['jointfile-art']) && !empty($_FILES['jointfile-art']) && !empty($_FILES['jointfile-art']['name'])) {

            $index = 'jointfile-art';
                $extensions = unserialize(K_ALLOWEXT);
            $nameFile = $_FILES['jointfile-art']['name'];

            $statusUpload = @uploadFile($index, $nameFile, PATHFILE, K_MAXSIZE, $extensions);

            if ($statusUpload == false){
                $jointFile = '';
                $process['status'] = false;
                $process['msg'] = T_("Le système a rencontré un problème lors de l'enregistrement du fichier joint"); 
            }else
                $jointFile = $nameFile;    

        }else
            $jointFile = '';
         */

        $con = new Model('film');
        $film = $con->db->prepare("
            INSERT
            INTO
            $con->table(
            titre_film,
            categorie_id, 
            support_id, 
            reference_film,
            resume_film, 
            picture_film,
            lien_film,
            acteur_film,
            date_prod_film,
            duree_film,
            auteur_film,
            vision_film,
            avis_film,
            commentaire_film
            ) 
            VALUES(
            :titre,
            :categorie, 
            :support, 
            :reference,
            :resume, 
            :picture,
            :lien,
            :acteur,
            :date,
            :duree,
            :auteur,
            :vision,
            :avis,
            :commentaire
            )
        ");
          
        $process['status'] = $film->execute([
            "titre" => htmlentities($titre),
            "categorie"  => $categorie,
            "support" => $support,
            "reference" => htmlentities($reference),
            "resume" => htmlentities($resume),
            "picture" => $image,
            "lien" => htmlentities($lien),
            "acteur" => htmlentities($acteurs),
            "date" => $annee_prod,
            "duree" => $duree,
            "auteur" => htmlentities($realisateur),
            "vision" => (empty($vision))? $vision = 1 : $vision = $vision,
            "avis" => (empty($cote))? $cotes = 1 : $cote = $cote,
            "commentaire" => htmlentities($commentaire)
        ]);

        // Si pas d'erreur
        if($process['status']){
            $process['msg'] = T_("Votre nouveau Film a été ajouté");    
        }else
            $process['msg'] = T_("Le système a rencontré un problème lors de l'enregistrement du Film");

        $film->closeCursor();

    }

    return $process;
}

/**
 * Crée et retourne une fiche Film vide (pour update.php)
 * @return string
 */
function getEmptyFilm() {
    
    $film = array();
    $film['id_film'] = null;
    $film['categorie_id'] = null;
    $film['support_id'] = null;
    $film['titre_film'] = '';
    $film['reference_film'] = '';
    $film['resume_film'] = '';
    $film['picture_film'] = '';;
    $film['lien_film'] = '';
    $film['acteur_film'] = '';
    $film['titre_categorie'] = '';
    $film['titre_support'] = '';
    $film['date_prod_film'] = '';
    $film['duree_film'] = '';
    $film['commentaire_film'] = '';
    $film['vision_film'] = 0;
    $film['avis_film'] = 1;
    $film['auteur_film'] = '';
    
    return $film;
    
}

/**
 * Retourne la fiche complète d'un film donné en paramètre
 * @param int $idfilm
 * @return array
 */
function getFicheFilm($idfilm) {
  
    $con = new Model('v_fiche_film');
    $film = $con->db->prepare("SELECT * FROM $con->table WHERE id_film = :idfilm");
    $film->bindParam(':idfilm', $idfilm, PDO::PARAM_INT);
    $film->execute();
    $film = $film->fetch(PDO::FETCH_ASSOC);
    return $film;
}

/**
 * Mise à jour de la fiche d'un Film
 * 
 * @param type $post
 * @param type $id
 * @param type $actuImage
 * @return type
 */
function updateFilm($post, $id, $actuImage) {

    // Initialisations
    extract($post);

    $validation = true;
    $status = false;
    $flagNewPicture = false;
    
    //DEBUG//print_r($post); die();   

    if(empty($titre) || empty($reference)) {
        $process['status'] = false;
        $process['msg'] = T_("Le titre et la référence sont obligatoires");
    } 

    if($validation) { 

        // Traitement de l'affiche du Film
            if(isset($_FILES['picture-art']) && !empty($_FILES['picture-art']) && !empty($_FILES['picture-art']['name'])) {
                $image = @uploadPicture($_FILES);
                if ($image == false){
                    $image = '';
                    $status = T_("Le système a rencontré un problème lors de l'enregistrement de l'affiche"); 
                } 
                $flagNewPicture = true;
            }else
                $image = $actuImage;  
            
        // Si on a demandé la suppression de l'affiche en cours ou modifier l'affiche
            if((isset($del_picture) && $del_picture) || $flagNewPicture) {
                
                //DEBUG//print("DEL ".ABSPATH.D_PRIVATE.DS.D_DATAS.DS.D_IMG.DS.D_ORIGINAL.DS.$actuImage);
                // Si c'est un fichier
                if(!empty($actuImage))
                {
                    //DEBUG//print(" FILE DELETED ");
                    $stDelImgOrigin = unlink(ABSPATH.D_PRIVATE.DS.D_DATAS.DS.D_IMG.DS.D_ORIGINAL.DS.$actuImage); 
                    $stDelImgThumb = unlink(ABSPATH.D_PRIVATE.DS.D_DATAS.DS.D_IMG.DS.D_THUMB.DS.$actuImage);
                }    
                
                // Mise à jour ou non du nom si on a changé en même temps l'affiche
                if(!$flagNewPicture)
                    $image = '';
            }
        
        /*
        // Traitement du fichier joint
        if(isset($_FILES['jointfile-art']) && !empty($_FILES['jointfile-art']) && !empty($_FILES['jointfile-art']['name'])) {

            $index = 'jointfile-art';
            $extensions = unserialize(K_ALLOWEXT);
            $nameFile = $_FILES['jointfile-art']['name'];

            $statusUpload = @uploadFile( $index, $nameFile, PATHFILE, K_MAXSIZE, $extensions );

            if ($statusUpload == false){
                $jointFile = '';
                $status = T_("Le système a rencontré un problème lors de l'enregistrement du fichier joint"); 
            }else
                $jointFile = $nameFile;    

        }else{
            // Si effacement
            if(isset($del_file) && $del_file) {
                $stDelFileThumb = unlink(ABSPATH.PATHFILE.$actuFichier);
                $jointFile = '';                
            }else
                $jointFile = $actuFichier;  
        }
        */

        // Update process    
        $con = new Model('film');    
        $modifier = $con->db->prepare("
            UPDATE $con->table
            SET 
            titre_film = :titre,
            categorie_id = :categorie_id, 
            support_id = :support_id, 
            reference_film = :reference,
            resume_film = :resume, 
            picture_film = :affiche,
            lien_film = :lien,
            acteur_film = :acteur,
            date_prod_film = :date,
            duree_film = :duree,
            commentaire_film = :commentaire,
            auteur_film = :realisateur,
            vision_film = :vision,
            avis_film = :cote
            WHERE 
            id_film = :id_film"
        );
        $modifier->execute([
           "titre" => htmlentities($titre),
           "categorie_id"  => $categorie,
           "support_id" => $support,
           "reference" => htmlentities($reference),
           "resume" => htmlentities($resume),
           "affiche" => $image,
           "lien" => htmlentities($lien),
           "acteur" => htmlentities($acteurs),
           "date" => $annee_prod,   
           "duree" => $duree,
           "commentaire" => $commentaire,
           "realisateur" => $realisateur,
           "vision" => (empty($vision))? $vision = 0 : $vision = $vision,
           "cote" => (empty($cote))? $cotes = 1 : $cote = $cote,
           "id_film" => $id
        ]); 
    }

    return $status;      

}

/**
* Supprimer un élément dans une table
* 
* @param string $table (La table ou l'item doit être supprimé)
* @param integer $id (L'id de l'item à supprimer)
* @return array (Renvoi un message)
*/
function deleteItem($table, $id) {

    $process = array();
    $process['status'] = true;
    $process['msg'] = NULL;

    $con = new Model($table);  
    $delete = $con->db->prepare("
        DELETE FROM $con->table 
        WHERE id_$table = :id"
    );  

    $delete->bindParam(':id', $id, PDO::PARAM_INT); 
    $process['status'] = @$delete->execute();
    $delete->closeCursor();

    // Si l'opération de suppression s'est déroulée correctement
    if($process['status']){
        
        switch ($table)
        {
            case 'film' :
                    $process['msg'] = T_("Le film a été supprimé");    
                    break;
            case 'categorie' :
                    $process['msg'] = T_("La categorie a été supprimée");   
                    break;
            case 'support' :
                    $process['msg'] = T_("Le support a été supprimé");   
                    break;            
        }
        
    }else
        $process['msg'] = T_("Le système a rencontré un problème lors de l'opération de suppression");
    
      
   return $process;

}


/**
 * Renvoi le code HTML de la modale de confirmation de suppression
 * @param type $titreFilm
 * @return string
 */
function modalDeleteFilmConfirm($titreFilm = null){
    
    $html='
       <div class="modal fade" id="modalFilmDeleteConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">{trm-suppression-film}</h4>
            </div>
            <div class="modal-body text-center">
              <h3>'.$titreFilm.'</h3>
            </div>
            <div class="modal-footer">
              <input type="hidden" id="id_film" name="id_film" value="{id_film}">
              <button type="button" class="btn btn-primary" data-dismiss="modal">{trm-annuler}</button>
              <button type="submit" class="btn btn-danger" id="supprimer" name="supprimer" value="supprimer">{trm-confirmer}</button>
             </div>
          </div>
        </div>
      </div>
    ';
    
    return $html;

}

/**
 * Fonction de recherche de Films retourne un tableau avec le(s) Film(s)
 * 
 * @param type $field
 * @param type $string
 * @return type
 */
function searchFilm($field, $string) {
    
    $con = new Model('v_liste_films_simple');
    $sql = "SELECT * FROM $con->table WHERE $field  LIKE '%$string%' ORDER BY $field ASC";
    $req = $con->db->query($sql);
    $count = $req->rowCount();
    //echo $count; die();
    if($count > 0) {
        $arrayFilms = $req->fetchall();
        $film_liste = array();        
        foreach ($arrayFilms as $film) {

             $film_liste[] = array(
                "id" => $film['id_film'],  
                "titre" => $film['titre_film'],
                "reference" => $film['reference_film']                        
            );
        }
    }else
        $film_liste = false;

    $req->closeCursor();
    return $film_liste;
}



