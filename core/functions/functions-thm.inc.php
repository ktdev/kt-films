<?php

//-----------------------------------------------------------------------------
/**
* Retourne le code HTML de la section HEAD
*  
* Certaines balises <meta>
* La balise <title>
* Le chargement des feuilles de styles <link>
* 
* @param string $pathTHEME
*/
function insertHeadSection($pathTHEME)
{


    $html = '
    <meta charset="UTF-8">
    <title>{titre}</title>
    <meta name="author" content="{author} | {author_site}">
    <meta name="creation-date" content="{creation_date}">
    <meta name="description" content="{description}">
    <meta name="keywords" content="{keywords}">
    <meta name="revisit-after" content="{revisit_after}">
    <meta name="robots" content="{robots}">                
    <!-- Responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicons -->
    <link rel="shortcut icon" type="image/x-icon" href="'.$pathTHEME.D_THM_USE.DS.'images'.DS.'favicons'.DS.'favicon.png" />
    <link rel="icon" type="image/x-icon" href="'.$pathTHEME.D_THM_USE.DS.'images'.DS.'favicons'.DS.'favicon.png" />
    <!-- CSS -->
    <link rel="stylesheet" href="'.$pathTHEME.D_CORE.DS.D_BOOTS.DS.K_BOOTS_VER.DS.D_CSS.DS.'bootstrap.min.css">
    <!--<link rel="stylesheet" href="'.$pathTHEME.D_CORE.DS.D_JS.DS.'jqueryui'.DS.K_JQUERYUI_VER.DS.'jquery-ui.min.css">-->
    <!--<link rel="stylesheet" href="'.$pathTHEME.D_CORE.DS.D_JS.DS.'jqueryui'.DS.K_JQUERYUI_VER.DS.'jquery-ui.theme.min.css">-->
    <link rel="stylesheet" href="'.$pathTHEME.D_CORE.DS.'font-awesome'.DS.D_CSS.DS.'font-awesome.min.css">
    <link rel="stylesheet" href="'.$pathTHEME.D_CORE.DS.'swipebox'.DS.D_CSS.DS.'swipebox.min.css">
    <!--<link rel="stylesheet" href="'.$pathTHEME.D_THM_USE.DS.D_CSS.DS.'styles-base.css">-->
    <link rel="stylesheet" href="'.$pathTHEME.D_THM_USE.DS.D_CSS.DS.D_THM_USE.'.css">
    ';
    return $html;
}

function insertCSSDatatables($pathTHEME){
    $html = '<link rel="stylesheet" href="'.$pathTHEME.D_THM_USE.DS.D_CSS.DS.'datatables.css">';
    return $html;
}

/**
* Retourne le code HTML du "footer" de l'application
* 
*/
function insertFooter(){

    $html ='
    <p class="foot text-center"><a href="'.K_URL_RELEASE.'">'.K_NAME.' '.K_VER.'</a> {trm-est} <i class="fa fa-code"></i> {trm-par} <a href="'.K_AUTHOR_SITE.'">'.K_AUTHOR.'</a> - '.K_YEAR.'  - Licence <a href="https://www.gnu.org/licenses/gpl.html">GPLv3</a></p>
    ';  

    return $html;
}



/**
* Retourne le code HTML du système
* de navigation principal de l'application
* 
*/
function includeNavigation($path)
{
    $incl = file_get_contents(ABSPATH.$path.'navigation.inc.php');
    return $incl;
}


/**
 * Retourne le code HTML de la section user de la navigation
 * 
 * @return string
 */
function insertMenuUser()
{

    $html = '';

    if(isset($_SESSION['IDENTIFY']) && $_SESSION['IDENTIFY'] == 0 )
        $html = '<li><a href="{url-home-login}">{menu-connecter}</a></li>';
    else{
        
        if(!isset($_SESSION['pseudo'])) $_SESSION['pseudo']  = T_('Inconnu');
        
        // Les accès d'un utilisateur normal part 1
        $html .= '
        <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> <span class="wYellowColor">'.$_SESSION['pseudo'].'</span> <span class="caret"></span></a>
        <ul class="dropdown-menu">
        <li><a class="sous-menu" href="{url-main-index}">{menu-main-index}</a></li>
        <li><a class="sous-menu" href="{url-main-list}">{menu-main-list}</a></li>
        ';
        
        // Si on est un administrateur / superuser (su)
        if(isset($_SESSION['su']) && $_SESSION['su'] == 1 ) {
            $html .= '
            <li role="separator" class="divider"></li>
            <li><a class="sous-menu" href="{url-admin-addfilm}">{menu-admin-addfilm}</a></li>
            <li><a class="sous-menu" href="{url-admin-index}">{menu-admin-index}</a></li>
            <li><a class="sous-menu" href="{url-admin-settings}">{menu-admin-settings}</a></li>                      
            ';    
        }       
        /*
        if(K_DEBUG)
            $html .= '<li><a href="{url-testing-teststore}"><span class="glyphicon glyphicon-alert wRedColor" aria-hidden="true"></span> Stores tests</a></li>';
        $html .= '<li role="separator" class="divider"></li>
         */
        // Les accès d'un utilisateur normal part 2
        $html .= '
            <li role="separator" class="divider"></li>
            <li><a class="sous-menu" href="{url-user-profile}">{menu-profile}</a></li>  
            <li role="separator" class="divider"></li>
            <li><a href="{url-home-logout}"><span class="wRedColor">{menu-quitter}</span></a></li>            
            </ul>
            </li>
        '; 
    }

    return $html;
}

/**
* Retourne le code HTML de la section langue de la navigation
* 
* @param string $query : Paramètres de la requète !!! La variable $query est initialisée dans le bootstrap
* 
*/
function insertMenuLang($query) {
    
    $checkedColorFr = '';
    $checkedColorEn = '';
    $checkedColorNl = '';
    $txtColorFr = '';
    $txtColorEn = '';
    $txtColorNl = '';
    
    // Supprimer les locales si déjà existantes dans l'url en vérifiant les 
    // sous-répertoires du répertoire locale
    $subFolders = KTgetSubFolder(ABSPATH.DS.D_CORE.DS.'locale');
    if($subFolders != false)
    foreach($subFolders as $key=>$value) {
         $query = str_replace('&locale='.$value, '', $query);        
    }
    

    if(!isset($_SESSION['locale'])) $_SESSION['locale']  = DEF_LOCALE;

    switch($_SESSION['locale']) {

        case 'fr_BE': 
            $checkedColorFr = '<i class="fa fa-check wGreenColor"></i>';
            $checkedColorEn = '<i class="fa fa-check"></i>';
            $checkedColorNl = '<i class="fa fa-check"></i>';
            $txtColorFr  = 'wGreenColor';
            $txtColorEn  = '';
            $txtColorNl  = '';
            break;
        case 'nl_BE': 
            $checkedColorFr = '<i class="fa fa-check"></i>';
            $checkedColorEn = '<i class="fa fa-check"></i>';
            $checkedColorNl = '<i class="fa fa-check wGreenColor"></i>';
            $txtColorFr  = '';
            $txtColorEn  = '';
            $txtColorNl  = 'wGreenColor';
            break;
        case 'en_US': 
            $checkedColorFr = '<i class="fa fa-check"></i>';
            $checkedColorEn = '<i class="fa fa-check wGreenColor"></i>';
            $checkedColorNl = '<i class="fa fa-check"></i>';
            $txtColorFr  = '';
            $txtColorEn  = 'wGreenColor';
            $txtColorNl  = '';
            break;
        default :
            $checkedColorFr = '<i class="fa fa-check wGreenColor"></i>';
            $checkedColorEn = '<i class="fa fa-check"></i>';
            $checkedColorNl = '<i class="fa fa-check"></i>';
            $txtColorFr  = 'wGreenColor';
            $txtColorEn  = '';
            $txtColorNl  = '';

    }

    $html = '
    <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{trm-langue} <span class="caret"></span></a>
    <ul class="dropdown-menu">
    <li><a href="?'.$query.'&locale=fr_BE">'.$checkedColorFr.' <span class="'.$txtColorFr.'">{menu-lang-fr}</span></a></li>
    <li><a href="?'.$query.'&locale=nl_BE">'.$checkedColorNl.' <span class="'.$txtColorNl.'">{menu-lang-nl}</span></a></li>
    <li><a href="?'.$query.'&locale=en_US">'.$checkedColorEn.' <span class="'.$txtColorEn.'">{menu-lang-en}</a></li>
    </ul>
    </li>
    ';

    return $html;
}



/**
* Retourne le code HTML (en bas de page) du chargement 
* de la Clase JS Chart (nécessaire pour la réaliser des Charts)
* 
* @param string $pathTHEME
*/
function insertJsChart($pathTHEME)
{
    $html ='

    <!-- Chart.js - http://www.chartjs.org (necessary for Chart) -->
    <script src="'.$pathInstall.DS.$pathTHEME.D_CORE.DS.D_JS.DS.'chart'.DS.'chart.min.js"></script>

    ';

    return $html;
}


/**
* Retourne le code HTML (en bas de page) du chargement des fichiers JS de base
* 
* @param string $pathTHEME
*/
function insertFooterJsSection($pathTHEME)
{

    $html = ' 
    <!-- jQuery (necessary for Bootstrap) -->
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'jquery'.DS.K_JQUERY_VER.DS.'jquery.min.js"></script>
    <script src="'.$pathTHEME.D_CORE.DS.'swipebox'.DS.D_JS.DS.'jquery.swipebox.min.js"></script>
    <!-- jQueryUI -->
    <!--<script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'jqueryui'.DS.K_JQUERYUI_VER.DS.'jquery-ui.min.js"></script>-->
    <!-- Bootstrap -->
    <script src="'.$pathTHEME.D_CORE.DS.D_BOOTS.DS.K_BOOTS_VER.DS.D_JS.DS.'bootstrap.min.js"></script>
    <!-- Wal Js -->
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'wal'.DS.'wal.js"></script>     
    ';
    return $html;
}

/**
* Retourne le code HTML (en bas de page) du chargement des fichiers JS du
* module dataTables de jQuery 
* 
* @param string $pathTHEME
*/
function insertFooterJsDatatables($pathTHEME)
{

    $html = '  
    <!-- DataTables for jQuery -->
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'datatables'.DS.'datatables.custom.js"></script> 
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'datatables'.DS.'datatables.bootstrap.min.js"></script>
    <script src="'.$pathTHEME.D_CORE.DS.D_JS.DS.'wal'.DS.'datatable.js"></script>  
            
    ';
    return $html;
}

/**
* Retourne le code HTML d'une div formatée en fonction des données reçues
* La variable innerSpan permet d'inclure une balise de type span qui
* permet d'afficher un message secondaire
* @param string $type  (ALERT, SUCCESS, WARNING)
* @param string $classes
* @param string $message
* @param string $role
* @param string $id
* @param string $innerSpan
*/
function KTMakeDiv( $type, $classes, $message, $role = null, $id = null, $innerSpan = null )
{
    Switch($type)
    {
        case 'ALERT':
            $div = '<div class="';           
            $div .= $classes.'"';
            $div .= 'id="'.$id.'" ';
            $div .= ' role="'.$role.'">';
            $div .= $message;
            if(isset($innerSpan)) $div .= '<span id="submsg">'.$innerSpan.'</span>';
            $div .= '</div>';

        case 'SUCCESS':
            $div = '<div class="';
            $div .= $classes.'" ';
            $div .= 'id="'.$id.'" ';
            $div .= ' role="'.$role.'">';
            $div .= $message;
            if(isset($innerSpan)) $div .= '<span id="submsg">'.$innerSpan.'</span>';
            $div .= '</div>';

        case 'WARNING':
            $div = '<div class="';
            $div .= $classes.'"';
            $div .= 'id="'.$id.'" ';
            $div .= ' role="'.$role.'">';
            $div .= $message;
            if(isset($innerSpan)) $div .= '<span id="submsg">'.$innerSpan.'</span>';
            $div .= '</div>';
    }


    return $div;
}


