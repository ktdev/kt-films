CREATE DEFINER=`DBA`@`localhost` PROCEDURE `ktfilms_CountCaractere`()
BEGIN

SELECT id_film, LENGTH(acteur_film) AS acteur_length, 
				LENGTH(titre_film) AS titre_length, 
                LENGTH(resume_film) AS resume_length,
                LENGTH(commentaire_film) AS commentaire_length,
                LENGTH(lien_film) AS lien_length  FROM ktfilms_film;
                

END