CREATE ALGORITHM = UNDEFINED DEFINER = `DBA`@`localhost` SQL SECURITY DEFINER
VIEW `ktfilms_v_fiche_film` AS
    SELECT 
        `ktfilms_film`.`id_film` AS `id_film`,
        `ktfilms_film`.`titre_film` AS `titre_film`,
        `ktfilms_film`.`reference_film` AS `reference_film`,
        `ktfilms_film`.`resume_film` AS `resume_film`,
        `ktfilms_film`.`auteur_film` AS `auteur_film`,
        `ktfilms_film`.`acteur_film` AS `acteur_film`,
        `ktfilms_film`.`duree_film` AS `duree_film`,
        `ktfilms_film`.`date_prod_film` AS `date_prod_film`,
        `ktfilms_film`.`picture_film` AS `picture_film`,
        `ktfilms_film`.`lien_film` AS `lien_film`,
        `ktfilms_film`.`avis_film` AS `avis_film`,
        `ktfilms_film`.`commentaire_film` AS `commentaire_film`,
        `ktfilms_film`.`categorie_id` AS `categorie_id`,
        `ktfilms_film`.`support_id` AS `support_id`,
        `ktfilms_categorie`.`titre_categorie` AS `titre_categorie`,
        `ktfilms_support`.`titre_support` AS `titre_support`
    FROM
        ((`ktfilms_film`
        JOIN `ktfilms_support` ON ((`ktfilms_support`.`id_support` = `ktfilms_film`.`support_id`)))
        JOIN `ktfilms_categorie` ON ((`ktfilms_categorie`.`id_categorie` = `ktfilms_film`.`categorie_id`)))
    ORDER BY `ktfilms_film`.`titre_film`