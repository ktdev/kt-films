CREATE ALGORITHM = UNDEFINED DEFINER = `DBA`@`localhost` SQL SECURITY DEFINER
VIEW `ktfilms_v_liste_films_simple` AS
    SELECT 
        `film`.`id_film` AS `id_film`,
        `film`.`titre_film` AS `titre_film`,
        `film`.`reference_film` AS `reference_film`,
        `film`.`categorie_id` AS `categorie_id`,
        `film`.`support_id` AS `support_id`,
        `categorie`.`titre_categorie` AS `titre_categorie`,
        `support`.`titre_support` AS `titre_support`
    FROM
        ((`film`
        JOIN `categorie` ON ((`categorie`.`id_categorie` = `film`.`categorie_id`)))
        JOIN `support` ON ((`support`.`id_support` = `film`.`support_id`)))
    ORDER BY `film`.`titre_film`