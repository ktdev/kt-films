-- MySQL dump 10.13  Distrib 5.7.14, for Win64 (x86_64)
--
-- Host: localhost    Database: kt_Films
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `ktfilms_categorie`
--

LOCK TABLES `ktfilms_categorie` WRITE;
/*!40000 ALTER TABLE `ktfilms_categorie` DISABLE KEYS */;
INSERT INTO `ktfilms_categorie` VALUES (1,'Action'),(2,'Animés'),(3,'Asiatique'),(4,'Aventure'),(5,'Bonus'),(6,'Comédie'),(7,'Documentaire'),(8,'Enfant'),(9,'Fantastique'),(10,'Histoire vraie'),(11,'Horreur'),(12,'Nanar'),(13,'Romantique'),(14,'Science-Fiction'),(15,'Séries'),(16,'Thriller'),(17,'Western'),(18,'Heroic Fantasy'),(19,'Drame');
/*!40000 ALTER TABLE `ktfilms_categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ktfilms_film`
--

LOCK TABLES `ktfilms_film` WRITE;
/*!40000 ALTER TABLE `ktfilms_film` DISABLE KEYS */;
/*!40000 ALTER TABLE `ktfilms_film` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ktfilms_params`
--

LOCK TABLES `ktfilms_params` WRITE;
/*!40000 ALTER TABLE `ktfilms_params` DISABLE KEYS */;
INSERT INTO `ktfilms_params` VALUES (1,'INSTALL','Ok'),(2,'PATH_INSTALL','/dev/kt-films/'),(3,'TXT_WELCOM','Cette application est actuellement en cours de développement.'),(4,'TXT_NOTE',NULL);
/*!40000 ALTER TABLE `ktfilms_params` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ktfilms_support`
--

LOCK TABLES `ktfilms_support` WRITE;
/*!40000 ALTER TABLE `ktfilms_support` DISABLE KEYS */;
INSERT INTO `ktfilms_support` VALUES (1,'Disque Dur Externe'),(2,'Disque Vidéo'),(3,'Serveur FTP / DLNA'),(4,'Site de Streaming');
/*!40000 ALTER TABLE `ktfilms_support` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ktfilms_users`
--

LOCK TABLES `ktfilms_users` WRITE;
/*!40000 ALTER TABLE `ktfilms_users` DISABLE KEYS */;
INSERT INTO `ktfilms_users` VALUES (1,'admin','admin@demo.com','$2y$10$ajGUIYXVGdXFCLcouapkB.RexBM.clbIn0mt5dxRNzd9QKFEW41Pu',1,1),(2,'demo','demo@demo.com','$2y$10$r8NsaGjTaSW8lroPv75Jf.UUARK8qfnZGOP4Brkx.m.T7WKEJJRw6',1,0);
/*!40000 ALTER TABLE `ktfilms_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'kt_Films'
--
/*!50003 DROP PROCEDURE IF EXISTS `ktfilms_CountCaractere` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`DBA`@`localhost` PROCEDURE `ktfilms_CountCaractere`()
BEGIN

SELECT id_film, LENGTH(acteur_film) AS acteur_length, 
				LENGTH(titre_film) AS titre_length, 
                LENGTH(resume_film) AS resume_length,
                LENGTH(commentaire_film) AS commentaire_length,
                LENGTH(lien_film) AS lien_length  FROM ktfilms_film;
                

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-01 10:31:50
