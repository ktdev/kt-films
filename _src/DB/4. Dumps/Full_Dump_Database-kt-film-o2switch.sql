/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ktfilms_categorie`
--

DROP TABLE IF EXISTS `ktfilms_categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ktfilms_categorie` (
  `id_categorie` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre_categorie` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_categorie`),
  UNIQUE KEY `id_categorie_UNIQUE` (`id_categorie`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ktfilms_categorie`
--

LOCK TABLES `ktfilms_categorie` WRITE;
/*!40000 ALTER TABLE `ktfilms_categorie` DISABLE KEYS */;
INSERT INTO `ktfilms_categorie` VALUES (1,'Action'),(2,'Animés'),(3,'Asiatique'),(4,'Aventure'),(5,'Bonus'),(6,'Comédie'),(7,'Documentaire'),(8,'Enfant'),(9,'Fantastique'),(10,'Histoire vraie'),(11,'Horreur'),(12,'Nanar'),(13,'Romantique'),(14,'Science-Fiction'),(15,'Séries'),(16,'Thriller'),(17,'Western'),(18,'Heroic Fantasy');
/*!40000 ALTER TABLE `ktfilms_categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ktfilms_film`
--

DROP TABLE IF EXISTS `ktfilms_film`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ktfilms_film` (
  `id_film` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre_film` varchar(250) DEFAULT NULL,
  `reference_film` varchar(45) DEFAULT NULL,
  `resume_film` longtext,
  `picture_film` varchar(250) DEFAULT NULL,
  `lien_film` varchar(250) DEFAULT NULL,
  `auteur_film` varchar(250) DEFAULT NULL,
  `acteur_film` varchar(250) NOT NULL,
  `duree_film` varchar(10) DEFAULT NULL,
  `date_prod_film` varchar(4) DEFAULT NULL,
  `avis_film` varchar(1) DEFAULT NULL,
  `commentaire_film` longtext,
  `categorie_id` int(10) unsigned NOT NULL,
  `support_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_film`),
  UNIQUE KEY `idfilm_UNIQUE` (`id_film`),
  KEY `fk_film_categorie_idx` (`categorie_id`),
  KEY `fk_film_support1_idx` (`support_id`),
  CONSTRAINT `fk_film_categorie` FOREIGN KEY (`categorie_id`) REFERENCES `ktfilms_categorie` (`id_categorie`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_film_support1` FOREIGN KEY (`support_id`) REFERENCES `ktfilms_support` (`id_support`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ktfilms_film`
--

LOCK TABLES `ktfilms_film` WRITE;
/*!40000 ALTER TABLE `ktfilms_film` DISABLE KEYS */;
/*!40000 ALTER TABLE `ktfilms_film` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ktfilms_params`
--

DROP TABLE IF EXISTS `ktfilms_params`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ktfilms_params` (
  `id_params` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(50) DEFAULT NULL,
  `value` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_params`),
  UNIQUE KEY `idparams_UNIQUE` (`id_params`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ktfilms_params`
--

LOCK TABLES `ktfilms_params` WRITE;
/*!40000 ALTER TABLE `ktfilms_params` DISABLE KEYS */;
INSERT INTO `ktfilms_params` VALUES (1,'INSTALL','Ok'),(2,'PATH_INSTALL','/dev/kt-films/'),(3,'TXT_WELCOM','Cette application est actuellement en cours de développement.'),(4,'TXT_NOTE',NULL);
/*!40000 ALTER TABLE `ktfilms_params` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ktfilms_support`
--

DROP TABLE IF EXISTS `ktfilms_support`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ktfilms_support` (
  `id_support` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre_support` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_support`),
  UNIQUE KEY `id_support_UNIQUE` (`id_support`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ktfilms_support`
--

LOCK TABLES `ktfilms_support` WRITE;
/*!40000 ALTER TABLE `ktfilms_support` DISABLE KEYS */;
INSERT INTO `ktfilms_support` VALUES (1,'Disque Dur Externe'),(2,'Disque Vidéo'),(3,'Serveur FTP / DLNA'),(4,'Site de Streaming');
/*!40000 ALTER TABLE `ktfilms_support` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ktfilms_users`
--

DROP TABLE IF EXISTS `ktfilms_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ktfilms_users` (
  `id_user` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(25) NOT NULL,
  `email` varchar(80) NOT NULL,
  `hpasswd` varchar(255) DEFAULT NULL,
  `actif` int(1) unsigned NOT NULL DEFAULT '1',
  `su` int(1) DEFAULT '0',
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `idusers_UNIQUE` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ktfilms_users`
--

LOCK TABLES `ktfilms_users` WRITE;
/*!40000 ALTER TABLE `ktfilms_users` DISABLE KEYS */;
INSERT INTO `ktfilms_users` VALUES (1,'admin','admin@demo.com','$2y$10$ajGUIYXVGdXFCLcouapkB.RexBM.clbIn0mt5dxRNzd9QKFEW41Pu',1,1),(2,'demo','demo@demo.com','$2y$10$r8NsaGjTaSW8lroPv75Jf.UUARK8qfnZGOP4Brkx.m.T7WKEJJRw6',1,0);
/*!40000 ALTER TABLE `ktfilms_users` ENABLE KEYS */;
UNLOCK TABLES;

---
-- Temporary view structure for view `ktfilms_v_fiche_film`
--

DROP TABLE IF EXISTS `ktfilms_v_fiche_film`;
/*!50001 DROP VIEW IF EXISTS `ktfilms_v_fiche_film`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `ktfilms_v_fiche_film` AS SELECT 
 1 AS `id_film`,
 1 AS `titre_film`,
 1 AS `reference_film`,
 1 AS `resume_film`,
 1 AS `auteur_film`,
 1 AS `acteur_film`,
 1 AS `duree_film`,
 1 AS `date_prod_film`,
 1 AS `picture_film`,
 1 AS `lien_film`,
 1 AS `avis_film`,
 1 AS `commentaire_film`,
 1 AS `categorie_id`,
 1 AS `support_id`,
 1 AS `titre_categorie`,
 1 AS `titre_support`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `ktfilms_v_liste_films_simple`
--

DROP TABLE IF EXISTS `ktfilms_v_liste_films_simple`;
/*!50001 DROP VIEW IF EXISTS `ktfilms_v_liste_films_simple`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `ktfilms_v_liste_films_simple` AS SELECT 
 1 AS `id_film`,
 1 AS `titre_film`,
 1 AS `reference_film`,
 1 AS `categorie_id`,
 1 AS `support_id`,
 1 AS `titre_categorie`,
 1 AS `titre_support`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'ktfilms_kt_Films'
--
/*!50003 DROP PROCEDURE IF EXISTS `ktfilms_CountCaractere` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`kentaro`@`localhost` PROCEDURE `ktfilms_CountCaractere`()
BEGIN

SELECT id_film, LENGTH(acteur_film) AS acteur_length, 
				LENGTH(titre_film) AS titre_length, 
                LENGTH(resume_film) AS resume_length,
                LENGTH(commentaire_film) AS commentaire_length,
                LENGTH(lien_film) AS lien_length  FROM ktfilms_film;
                

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `ktfilms_v_fiche_film`
--

/*!50001 DROP VIEW IF EXISTS `ktfilms_v_fiche_film`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`kentaro`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ktfilms_v_fiche_film` AS select `ktfilms_film`.`id_film` AS `id_film`,`ktfilms_film`.`titre_film` AS `titre_film`,`ktfilms_film`.`reference_film` AS `reference_film`,`ktfilms_film`.`resume_film` AS `resume_film`,`ktfilms_film`.`auteur_film` AS `auteur_film`,`ktfilms_film`.`acteur_film` AS `acteur_film`,`ktfilms_film`.`duree_film` AS `duree_film`,`ktfilms_film`.`date_prod_film` AS `date_prod_film`,`ktfilms_film`.`picture_film` AS `picture_film`,`ktfilms_film`.`lien_film` AS `lien_film`,`ktfilms_film`.`avis_film` AS `avis_film`,`ktfilms_film`.`commentaire_film` AS `commentaire_film`,`ktfilms_film`.`categorie_id` AS `categorie_id`,`ktfilms_film`.`support_id` AS `support_id`,`ktfilms_categorie`.`titre_categorie` AS `titre_categorie`,`ktfilms_support`.`titre_support` AS `titre_support` from ((`ktfilms_film` join `ktfilms_support` on((`ktfilms_support`.`id_support` = `ktfilms_film`.`support_id`))) join `ktfilms_categorie` on((`ktfilms_categorie`.`id_categorie` = `ktfilms_film`.`categorie_id`))) order by `ktfilms_film`.`titre_film` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ktfilms_v_liste_films_simple`
--

/*!50001 DROP VIEW IF EXISTS `ktfilms_v_liste_films_simple`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`kentaro`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ktfilms_v_liste_films_simple` AS select `ktfilms_film`.`id_film` AS `id_film`,`ktfilms_film`.`titre_film` AS `titre_film`,`ktfilms_film`.`reference_film` AS `reference_film`,`ktfilms_film`.`categorie_id` AS `categorie_id`,`ktfilms_film`.`support_id` AS `support_id`,`ktfilms_categorie`.`titre_categorie` AS `titre_categorie`,`ktfilms_support`.`titre_support` AS `titre_support` from ((`ktfilms_film` join `ktfilms_categorie` on((`ktfilms_categorie`.`id_categorie` = `ktfilms_film`.`categorie_id`))) join `ktfilms_support` on((`ktfilms_support`.`id_support` = `ktfilms_film`.`support_id`))) order by `ktfilms_film`.`titre_film` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;


-- Dump completed on 2017-09-01 10:30:56
