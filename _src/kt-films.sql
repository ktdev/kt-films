-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 21 Août 2017 à 17:30
-- Version du serveur :  5.5.57-0+deb8u1
-- Version de PHP :  5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `KT-FILMS`
--

-- --------------------------------------------------------

--
-- Structure de la table `films`
--

CREATE TABLE IF NOT EXISTS `films` (
`id_film` int(10) unsigned NOT NULL,
  `titre_film` varchar(250) NOT NULL,
  `categorie_film` varchar(50) NOT NULL,
  `support_film` varchar(25) NOT NULL,
  `reference_film` varchar(50) NOT NULL,
  `resume_film` text NOT NULL,
  `picture_film` varchar(250) NOT NULL,
  `lien_film` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `films`
--

INSERT INTO `films` (`id_film`, `titre_film`, `categorie_film`, `support_film`, `reference_film`, `resume_film`, `picture_film`, `lien_film`) VALUES
(1, 'TEST 1', 'Action', 'Disque Dur Externe', '001', 'Hello World', 'Avatars.png', 'www.ktdev.pro'),
(2, 'TEST 2', 'Bonus', 'Site de Streaming', '002', 'Hello World 2', 'raspberry-pi.png', 'www.ktdev.pro');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `films`
--
ALTER TABLE `films`
 ADD PRIMARY KEY (`id_film`), ADD KEY `titre_film` (`titre_film`(191));

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `films`
--
ALTER TABLE `films`
MODIFY `id_film` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
