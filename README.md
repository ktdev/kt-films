# KT-FILMS - WebApp de gestion de films

</> by KTDev - [www.ktdev.pro](https://www.ktdev.pro)

Version : 0.5.1

## Présentation
KT-Films est une web application dont l'objectif est l'organisation et la gestion de films.

Statut actuel : En cours de développement - alpha version

## Installation

Actuellement KT-FILMS ne possède pas d'installateur intégré permettant d'automatiser l'installation de l'application.

Il est donc nécessaire d'effectuer certaines opérations de façon manuelle.

### Procédure 

- Envoyer les fichiers de l'application vers votre serveur web (à l'exception du répertoire source : _src, ce dernier n'étant pas nécessaire au fonctionnement)
- Créer la base de données à l'aide du fichier sql présent dans le répertoire: /_src/kt-films.sql (adapter éventuellement le nom de la DB en fonction de votre projet)
- Adapter le fichier .htacess en fonction de votre serveur web
- Modifier le chemin d'accès à l'application - Exemple:  RewriteBase </<dir>/kt-films/> (<dir> est optionnel cet exemple indique que kt-films est dans un sous répertoire par rapport à la racine de votre serveur web)
