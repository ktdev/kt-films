<?php

//-----------------------------------------------------------------------------
// Definit Bootstrap
if (! defined ( 'K_BOOTSTRAP' )) define ( 'K_BOOTSTRAP', 'if (!isset ( $_SESSION [\'bootstrap\'] )) $_SESSION [\'bootstrap\'] = TRUE;' );
// Définit le directory separator si pas définit (passage direct par bootstrap.php)
if (! defined ( 'DS' )) define ( 'DS',  DIRECTORY_SEPARATOR );
// Definit ABSPATH comme répertoire racine du site
if (! defined ( 'ABSPATH' )) define ( 'ABSPATH', dirname ( __FILE__ ) . DS );
//-----------------------------------------------------------------------------
// REQUIRE App et Params configuration files
require_once 'private'.DS.'conf.params.php';
require_once 'conf.app.php';

// BOOTSTRAP INIT
eval( K_BOOTSTRAP );

// INSTALLATION verification
(KTInstaller( 'CTRL_INSTALL' )) ? $_SESSION['startAndStop'] = true : $_SESSION['startAndStop'] = false;

// Si L'application est installée
if( $_SESSION['startAndStop'])
{
    try{
        // 1. Charger les paramètres de l'application

        $listParams = KTloadParams();

        // 2. Récupére certains paramètres
        $pathInstall = KTFindParam( $listParams, 'PATH_INSTALL' );
        // Globalisation du chemin d'installation
        $G_PATH_INSTALL = $pathInstall;

        // Initialisation et globalisation du texte d'intrtoduction et de la note
        $G_TXT_WELCOM = KTFindParam( $listParams, 'TXT_WELCOM' );
        $G_TXT_NOTE = KTFindParam( $listParams, 'TXT_NOTE' );

        // Initialiser une constante avec le pathInstall
        if (! defined ( 'K_PTH_INST' )) define ( 'K_PTH_INST', $pathInstall );
        // Créer les chemins d'accès vers les modules
        if (! defined ( 'K_PTH_MODS' )) define ( 'K_PTH_MODS', K_PTH_INST.D_APP.DS.D_MODS.DS);
        // Créer les chemins d'accès vers le répertoire themes
        if (! defined ( 'K_PTH_THM' )) define ( 'K_PTH_THM', D_THEMES.DS );
        // Créer les chemins d'accès vers le Thème en cours
        if (! defined ( 'K_PTH_THM_USE' )) define ( 'K_PTH_THM_USE', K_PTH_INST.D_THEMES.DS.D_THM_USE.DS );


        // 3. Supprimer le chemin d'installation éventuel de la requête
        // Si l'application est installée à la racine

        if($pathInstall == '/')
        {
            $request = $_SERVER ['REQUEST_URI'];


            // Supprime le premier slash '/' de la requête
            if($request != "/")
                $request = substr($request, 1); 


            // Sinon on supprimer le chemin d'installation    
        }else
            $request = str_replace( $pathInstall, "", $_SERVER ['REQUEST_URI'] );

        // PROTECTION DES POST
        // Vérification du Referer pour les variables passées en POST
        /*
            if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != '' && substr($_SERVER['HTTP_REFERER'], 7, strlen($_SERVER['SERVER_NAME'])) != $_SERVER['SERVER_NAME'])
            {
             $_POST = array();             
            }
        */
        // 4. Lancement du Router
        // Parsage de l'url
        $urlParsed = parse_url($request);
        //DEBUG// var_dump($urlParsed); 

        // Récupérer les clés path et query (si existantes) 
        $path = KTFindKey( $urlParsed, 'path' );
        $query = KTFindKey( $urlParsed, 'query' );    
        //DEBUG//echo($path);

        // Si l'uri correspond à la racine du site on demande d'afficher le module/page par défaut 
        if(empty($path) || $path == '/'){
            $GLOBALS['URI_ROOT'] = true;
            $GLOBALS['G_ROUTER'] = new Router( K_DEF_INDEX, M_INCL, $query );
            //Sinon on demande au router d'inclure la page
        }else{
            $GLOBALS['URI_ROOT'] = false;
            $GLOBALS['G_ROUTER']  = new Router( $path, M_INCL, $query );
        }
    }catch(Exception $e){
        echo 'Exception reçue : ',  $e->getMessage(), "\n";
    }
   
}else{
    echo "Veuillez lancer l'installation de l'application";
}

