<?php

//-----------------------------------------------------------------------------
 
    /* ++++++++++++++++++++++++++++ */
    /* +++  Constantes Globales +++ */
    /* ++++++++++++++++++++++++++++ */
    
    // Déclaration des variables globales
    global 
    $G_TXT_WELCOM , 
    $G_TXT_NOTE,
    $G_ROUTER,
    $G_PRISON,
    $G_TXT_PATH_INSTALL,
    $URI_ROOT;


    /* ++++++++++++++++++++++++++++++ */
    /* +++    Constantes System   +++ */
    /* ++++++++++++++++++++++++++++++ */
   
    // Définit Core Folder
    if (! defined ( 'D_CORE' )) define ( 'D_CORE', 'core' );

    // Définit App Folder
    if (! defined ( 'D_APP' )) define ( 'D_APP', 'app' );

    // Définit App modules Folder
    if (! defined ( 'D_MODS' )) define ( 'D_MODS', 'mods' );
    
    // Définit themes modules Folder
    if (! defined ( 'D_THM' )) define ( 'D_THM', 'themes' );
    
    // Définit App stores Folder
    if (! defined ( 'D_STORES' )) define ( 'D_STORES', 'stores' );

    // Définit Module Home Folder
    if (! defined ( 'D_HOME' )) define ( 'D_HOME', 'home' );

    // Définit Module back Folder (Backend Management)
    if (! defined ( 'D_BACK' )) define ( 'D_BACK', 'back' );
    
    // Définit Module admin Folder (Administrator Management)
    if (! defined ( 'D_ADM' )) define ( 'D_ADM', 'adm' );
    
    // Définit Module user Folder 
    if (! defined ( 'D_USER' )) define ( 'D_USER', 'user' );

    // Définit Misc Folder
    if (! defined ( 'D_MISC' )) define ( 'D_MISC', 'misc' );
    
    // Définit Help Folder
    if (! defined ( 'D_HELP' )) define ( 'D_HELP', 'help' );
    
    // Définit Misc Folder
    if (! defined ( 'D_MISC' )) define ( 'D_MISC', 'misc' );
    
    // Définit Libs Folder
    if (! defined ( 'D_LIBS' )) define ( 'D_LIBS', 'libs' );
    
    // Définit le testing Folder (module de tests)
    if (! defined ( 'D_TESTING' )) define ( 'D_TESTING', 'testing' );
    
    // Définit le Private Folder
    if (! defined ( 'D_PRIVATE' )) define ( 'D_PRIVATE', 'private' );
    
    // Définit includes Folder  (for inclusion snippet code)
    if (! defined ( 'D_INC' )) define ( 'D_INC', 'includes' );

    // Définit Tpl Folder
    if (! defined ( 'D_TPL' )) define ( 'D_TPL', 'tpl' );
    
    // Définit répertoire functions
    if (! defined ( 'D_FCT' )) define ( 'D_FCT', 'functions' );
    
    // Définit répertoire functions
    if (! defined ( 'D_CLAS' )) define ( 'D_CLAS', 'classes' );
    
    // Définit datas Folder
    if (!defined('D_DATAS')) {
        define('D_DATAS', 'datas');
    }

    // Définit images Folder
    if (!defined('D_IMG')) {
        define('D_IMG', 'pictures');
    }

    // Définit original Folder
    if (!defined('D_ORIGINAL')) {
        define('D_ORIGINAL', 'original');
    }

    // Définit thumb Folder
    if (!defined('D_THUMB')) {
        define('D_THUMB', 'thumb');
    }

    // Définit files Folder
    if (!defined('D_FILES')) {
        define('D_FILES', 'files');
    }

       
    // Définit les constantes du mode du routeur
    if (! defined ( 'M_INCL' )) define ( 'M_INCL', 'INCL' );
    if (! defined ( 'M_REDIR' )) define ( 'M_REDIR', 'REDIR' );
    
    /* +++++++++++++++++++++++++++++++++++ */
    /* +++    Constantes Application   +++ */
    /* +++++++++++++++++++++++++++++++++++ */
    
    // Définit le nom de l'application
    if(!defined('K_NAME')) define( 'K_NAME','KT-FILMS');
    
     // Definit Numéro de version de l'application
    if(!defined('K_VER')) define( 'K_VER', '0.5.1a');
    
    // Definit l'url de publication des releases
    if(!defined('K_URL_RELEASE')) define( 'K_URL_RELEASE', 'http://sources.xoransorvor.be:3000/KTDev/KT-FILMS/releases');
    
    // Definit l'année du lancement du développement de l'app 
    if(!defined('K_YEAR')) define( 'K_YEAR', '2017');
   
    // Définit le titre générique des page de l'application
    if(!defined('K_TITLE')) define( 'K_TITLE',K_NAME.' '.K_VER); 
    
    // Auteur de l'application 
    if(!defined('K_AUTHOR')) define( 'K_AUTHOR','KTDev');

    // Site web de l'auteur de l'application 
    if(!defined('K_AUTHOR_SITE')) define( 'K_AUTHOR_SITE','https://www.ktdev.pro');

    // Description de l'application (SEO)
    if(!defined('K_DESCRIPTION')) define( 'K_DESCRIPTION', K_NAME.' - Video library');

    // Keywords de l'application (SEO)
    if(!defined('K_KEYWORDS')) define( 'K_KEYWORDS','ktdev, films, movies, gestion, vidéothèque, video, library');   

    // REVISIT-AFTER de l'application (SEO)
    if(!defined('K_REVISIT_AFTER')) define( 'K_REVISIT_AFTER', '3 days');

    // Date de création de l'application (SEO)
    if(!defined('K_DATE_CREATION')) define( 'K_DATE_CREATION', '25/11/2017');

    // ROBOTS de l'application (SEO)
    if(!defined('K_ROBOTS')) define( 'K_ROBOTS', 'index,follow');


    /* ++++++++++++++++++++++++++++++ */
    /* +++    Constantes  Thème   +++ */
    /* ++++++++++++++++++++++++++++++ */

    // Définit Bootstrap  Version
    if (! defined ( 'K_BOOTS_VER' )) define ( 'K_BOOTS_VER', '3.3.6' );

    // Définit Jquery  Version
    if (! defined ( 'K_JQUERY_VER' )) define ( 'K_JQUERY_VER', '1.12.0' );

    // Définit JqueryUI  Version
    if (! defined ( 'K_JQUERYUI_VER' )) define ( 'K_JQUERYUI_VER', '1.11.4' );
    
    // Définit JQwidget  Version
    if (! defined ( 'K_JQWIDGET_VER' )) define ( 'K_JQWIDGET_VER', '4.0.0' );

    // Définit Bootstrap  Folder
    if (! defined ( 'D_BOOTS' )) define ( 'D_BOOTS', 'bootstrap' );

    // Définit Themes Folder
    if (! defined ( 'D_THEMES' )) define ( 'D_THEMES', 'themes' );

    // Définit CSS Folder
    if (! defined ( 'D_CSS' )) define ( 'D_CSS', 'css' );

    // Définit JS Folder
    if (! defined ( 'D_JS' )) define ( 'D_JS', 'js' );
    
    // Définit images Folder
    if (! defined ( 'D_IMG' )) define ( 'D_IMG', 'images' );   
    
    // Définit le chemin et le  fichier index par défaut au lancement de l'application
    if (! defined ( 'K_DEF_INDEX' )) define ( 'K_DEF_INDEX', D_APP . DS . D_MODS . DS . D_HOME . DS . 'login' );
    
    /* +++++++++++++++++++++++++++ */
    /* +++    Load next file   +++ */
    /* +++++++++++++++++++++++++++ */
    require_once D_CORE . DS . 'loading.inc.php';

