<?php

//-----------------------------------------------------------------------------

define( 'KT_ROOT', getcwd() );
define( 'DS', DIRECTORY_SEPARATOR );
try{
    // Chargement du bootstrap
    require KT_ROOT . DS . 'bootstrap.php';    

}catch(Exception $e){
    print_r($e);
}