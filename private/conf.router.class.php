<?php


//-----------------------------------------------------------------------------

/**
*  Le routeur n'acceptera de distribuer uniquement les modules et les vues
*  qui seront déclarés dans ce fchiers d'intialisation et dans leur liste
*  respective. Ceci ne comprends pas les slugs qui eux sont stockés dans la
*  base de données.
*/

Class ConfRouter
{
    /**
    * Nom du répertoire application par défaut
    * 
    */
    protected $safeApplicationDirectory = array( D_APP );

    /**
    * Nom du répertoire module par défaut
    * 
    */
    protected $safeModuleDirectory = array( D_MODS, D_STORES );

    /**
    * Liste des modules reconnus par le routeur de l'application
    * /app/mods/<nom_du_module>
    */
    protected $safeModulesList = array(
        D_HOME,
        D_BACK,
        D_USER,
        D_ADM,
        D_MISC,
        D_HELP,
        D_TESTING,
        D_PRIMARY,
        D_SECONDARY,
        D_TERTIARY        
    );

    /**
    * Liste des vues reconnues par le routeur de l'application
    * /app/mods/<nom_du_module>/<nom_de_La_vue>
    * 
    */
    protected $safeViewsList = array(
        "index",
        "list",
        "fiche",
        "teststore",
        "profile",
        "login",
        "logout",
        "settings",
        "edit-profile",
        "addfilm",
        "updfilm",
        "searchbycat",
        "noaccess"
    );
    
    /**
    * Liste des modules librement accessible
    * Aucune identification nécessaire
    * Par défaut les nouveaux modules créés sont privés
    * Donne l'accès libre au(x) module(s) suivants :
    * 
    */
    protected $freeAccessModule = array(
        D_HOME,
        D_HELP,
        D_MISC
    );
}
