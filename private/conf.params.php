<?php


//-----------------------------------------------------------------------------

    // Définit la langue par défaut (fr_FR, en_US)
    if (! defined ( 'DEF_LOCALE' )) define ( 'DEF_LOCALE', 'fr_BE' );    
      
    // Définit le thème utilisé par défaut
    if (! defined ( 'D_THM_USE' )) define ( 'D_THM_USE', 'wally' ); 

    // Active ou désactive le mode Debug (TRUE or FALSE)
    if (! defined ( 'K_DEBUG' )) define ( 'K_DEBUG', true ); 
    
    // Définit le paramètre cost de la fonction de hachage
    if (! defined ( 'COST_HASH' )) define ( 'COST_HASH', 10 );
    
    // Définit le répertoire du module principal
    if (! defined ( 'D_PRIMARY' )) define ( 'D_PRIMARY', 'film' );

    // Définit le répertoire du module secondaire : Backend
    if (! defined ( 'D_SECONDARY' )) define ( 'D_SECONDARY', 'backend' );
    
    // Définit le répertoire du module tertiaire : Admin
    if (! defined ( 'D_TERTIARY' )) define ( 'D_TERTIARY', 'admin' );

    // Configure le niveau d'erreurs en fonction du mode de debugging
    if (K_DEBUG) error_reporting(E_ALL | E_STRICT); else error_reporting(0);
    
    /* ++++++++++++++++++++++++++ */
    /* +++    FILE UPLOAD     +++ */
    /* ++++++++++++++++++++++++++ */
    
    // Definit la taille maximum des fichiers téléversés (1Mo = 1048576)
    if(!defined('K_MAXSIZE')) define( 'K_MAXSIZE', 1048576 * 2);
    
    // Definit les extension autorisées pour les fichiers téléversés 
    $allowExtensions = array('png', 'jpg');
    if(!defined('K_ALLOWEXT')) define( 'K_ALLOWEXT', serialize ($allowExtensions) );  

   
   
  